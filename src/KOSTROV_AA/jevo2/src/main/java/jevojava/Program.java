package jevojava;

/**
 * Created by Alexander on 17.03.2017.
 */
public class Program {
    
    public static void main(String[] args){
        Automata auton = new Automata();
        auton.on(); //автомат включен
        auton.printState();
        System.out.println(auton.printMenu());
        auton.coin(150); //положили денежку
        System.out.println(auton.getStatusS());
        auton.choice(3);
        System.out.println(auton.getStatusS());
        auton.check();
        if (auton.getState().toString()=="COOK")
           System.out.println("Ваш " + auton.getUserChoice() + " готов. Приходите ещё!");
        else
           System.out.println("Недостаточно средств");
        auton.finish();
        System.out.println(auton.getStatusS());
    }
}
