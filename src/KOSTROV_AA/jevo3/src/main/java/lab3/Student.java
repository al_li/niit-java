package lab3;

import java.util.Arrays;

/**
 * Created by Alexander on 28.03.2017.
 */
public class Student {
    private int id; // номер зачетки студента
    private String fio; // ФИО студента
    int[] marks = new int[5];
    private int num; // количество оценок
    public Group group; //ссылка на группу
    private String strokeFile;

    public Student(int id, String fio){
        this.id = id;
        this.fio = fio;
        }

    public void setId(int id){this.id = id;}
    public void setFio(String fio){this.fio = fio;}
    public void setNum() {
        for (int i = 0; i < this.marks.length; i++) {
            if (this.marks[i] > 1)
                this.num++;
        }
    }
    public void setGroup(Group group){this.group = group;}


    public Group getGroup(){return group;}
    public String getFio(){ return fio;}
    public int getNum(){return num;}
    public int getId() {return id;}
    public int[] getMarks(){return marks;}
    public String getMarksS() {
        String mar="";
        for(int i : marks){
            if (i>1)
                mar+=i + " ";
        }
        return mar;}

    static void procedure() throws IllegalAccessException {
        System.out.println("inside procedure");
        throw new IllegalAccessException("demo");
    }

    public void addMark(int i,int mark){
        if (mark > 1) {
            marks[i] = mark;
            this.num++;
        }
    };

    public float averageMark(){
        float sum = 0;
        int counter = 0;
        for(int i = 0; i<this.marks.length;i++){
            if (this.marks[i]>1) {
                sum += this.marks[i];
                counter++;
            }
        }
        return  sum/counter;
    }

}
