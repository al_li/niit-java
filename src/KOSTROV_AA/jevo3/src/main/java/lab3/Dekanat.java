package lab3;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import static java.lang.Integer.parseInt;

/**
 * Created by Alexander on 28.03.2017.
 */
public class Dekanat {
    Student[] students = new Student[90];
    Group[] groups = new Group[5];
    int[] marksi;
    int[] jacta;
    int maxIndex;
    int counter;

    public void headCreates() throws NullPointerException {

        try {
            for (int i = 0; i < 4; i++) {
                jacta = new int[90];
                maxIndex = 0;
                counter = 0;
                for (int s = 0; s < students.length; s++) {
                    if (students[s] != null) {
                        if (students[s].getGroup().getId() == groups[i].getId()) {
                            jacta[counter] = (int) s;
                            if (jacta[maxIndex] < jacta[counter])
                                maxIndex = counter;
                            counter++;
                        }
                    }

                }
              //  System.out.println(Arrays.toString(jacta));
                groups[i].setHead(students[jacta[((int) (Math.random() * counter))]]);
            }
        }
        catch (NullPointerException ex) {
                System.out.println(ex.getMessage());
            }
    }

    public void output(){
        try {
            for (Student i : students) {
                if (i != null)
                    System.out.println(" Студент: " + i.getFio() + " Группа: " + i.getGroup().getTitle() + " Оценки: " + i.getMarksS());
            }
        } catch(NullPointerException ex){
            System.out.println(ex.getMessage());
        }

        try {
            for (Group g : groups) {
                if (g != null)
                    System.out.println("Староста группы " + g.getTitle() + ": " + g.getHead().getFio());
            }
        } catch(NullPointerException ex){
            System.out.println(ex.getMessage());
        }
    }

    /*
    public Group createGroup(String name, int id){
        return new Group(name,id);
    }
    */

    public Student createStudent(int id,String name){
        return new Student(id, name);
    }

    public void changeGroup(Student student, Group newGroup){
        student.setGroup(newGroup);
    }

    public void outStudent(int number){
        students[number] = null;
    }

    public void saveToFile(File fileStudents, File fileGroups){
        JSONObject object = new JSONObject();
        JSONArray studentss = new JSONArray();
        object.put("students", studentss);
        try {
            for (int i = 0; i < students.length; i++) {


                if (students[i] instanceof Student) {
                    JSONObject object2 = new JSONObject();
                    object2.put("fio", students[i].getFio());
                    object2.put("num", students[i].getNum());
                    object2.put("id", students[i].getId());
                    object2.put("group", students[i].group.getId());
                    JSONArray marks = new JSONArray();
                    marksi = students[i].getMarks();
                    for (int ii = 0; ii < students[i].getMarks().length; ii++) {

                        if (marksi[ii] > 1)
                            marks.add(marksi[ii]);
                    }


                    object2.put("marks", marks);

                    studentss.add(object2);
                }

                FileWriter writer = new FileWriter(fileStudents);
                writer.write(object.toJSONString());
                writer.flush();
                writer.close();
            }
        }
            catch (NullPointerException ex) {
                System.out.println("Проблема с NULLом " + ex.getMessage());
            }
            catch (IOException ex) {
            System.out.println("Проблема с файлом " + ex.getMessage());
        }
        // записываем файл с группами

        JSONObject objectG = new JSONObject();
        JSONArray groupss = new JSONArray();
        objectG.put("groups", groupss);
        for (int i = 0; i < groups.length; i++) {

            if (groups[i] instanceof Group) {
                JSONObject object2 = new JSONObject();
                object2.put("name", groups[i].getTitle());
                object2.put("id", groups[i].getId());
                groupss.add(object2);
            }
        }
        try {
            FileWriter writer = new FileWriter(fileGroups);
            writer.write(objectG.toJSONString());
            writer.flush();
            writer.close();
        } catch (IOException ex) {
            System.out.println("Проблема с файлом " + ex.getMessage());
        }


    }


}
