package lab3;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;




import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static java.lang.Integer.parseInt;

//import libs.main.Mylib;


public class App
{
    public static void main( String[] args ) throws Exception
    {
        int t;
        Student[] studentsFF = new Student[100]; // Сохраняем данные студентов из файла в эту переменную
        Group[] groupsFF = new Group[5]; // Сохраняем данные студентов из файла в эту переменную
        Dekanat uni = new Dekanat();

        File fileStudents = new File("students.json");
        File fileGroups = new File("groups.json");
        try {
            //  формируем группы
            JSONParser parserG = new JSONParser();
            JSONObject object = (JSONObject) parserG.parse(new FileReader(fileGroups));
            JSONArray items=(JSONArray)object.get("groups");

            for(int n = 0; n < items.size(); n++)
            {
                JSONObject halfG = (JSONObject)items.get(n);
                groupsFF[n] = new Group((String)halfG.get("name"),parseInt(halfG.get("id").toString()));

            }

         }
        catch (IOException ex) {
            System.out.println("ИО ЕКСЕПТИОН" + ex.getMessage());
            }
            /*
        catch (ParseException ex) {
            System.out.println("ПАРС ексептион" + ex.getMessage());
            }
            */

        // формируем студентов из данных файла

        JSONParser parser = new JSONParser();
        try {
            JSONObject object2 = (JSONObject) parser.parse(new FileReader(fileStudents));
            JSONArray items2=(JSONArray)object2.get("students");
            for(int n = 0; n < items2.size(); n++)
            {
                JSONObject friend = (JSONObject)items2.get(n);
                studentsFF[n] = uni.createStudent(parseInt(friend.get("id").toString()),(String)friend.get("fio"));
                studentsFF[n].group = groupsFF[parseInt(friend.get("group").toString())-1];
                               //parseInt(friend.get("num").toString())

                JSONArray items3=(JSONArray)friend.get("marks");
                for(int i = 0; i < items3.size(); i++)
                    studentsFF[n].addMark(i, parseInt(items3.get(i).toString()));
             //   studentsFF[n].setNum();
                   // System.out.println(Arrays.toString(studentsFF[n].getMarks()));
                // заносим в массив группы студента - составляем группу студентов
                groupsFF[studentsFF[n].group.getId()-1].addStudent(studentsFF[n]);

            }

            } catch (IOException ex) {
                System.out.println("ИО ЕКСЕПТИОН" + ex.getMessage());
            }
            /*
            catch (ParseException ex) {
                System.out.println("ПАРС ексептион" + ex.getMessage());
            }
            */
        uni.students = studentsFF;
        uni.groups = groupsFF;
        uni.groups[3]= new Group("Archeologi",4);
        uni.students[1] = uni.createStudent(1,"Kostrov.A.A");
        uni.students[1].addMark(0,5);
        uni.students[1].addMark(1,5);
        uni.groups[3].addStudent(uni.students[1]);
        uni.changeGroup(studentsFF[3],groupsFF[2]);  //меняем группу студенту
      //  uni.outStudent(0); //отчисляем студента
        uni.saveToFile(fileStudents,fileGroups);
        uni.headCreates();
        System.out.println("Средний балл группы : " +  groupsFF[0].getTitle() + "-" + groupsFF[0].getAverageMarks());
        System.out.println("Средний балл группы : "  + groupsFF[1].getTitle() + "-" + groupsFF[1].getAverageMarks());
        System.out.println("Средний балл группы : "  + groupsFF[2].getTitle() + "-" + groupsFF[2].getAverageMarks());
        System.out.println("Средний балл группы : "  + groupsFF[3].getTitle() + "-" + groupsFF[3].getAverageMarks());
        System.out.println("Поиск студента по id 10 " + groupsFF[0].search(10).getFio());
        System.out.println("Вывод средней оценки студента " + studentsFF[1].getFio() + ": " + studentsFF[1].averageMark());
        uni.output();




    }
}
