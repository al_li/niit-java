package jevo;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Integer.parseInt;

public class App
{

    static JSONArray toArray() throws Exception{
        File fileEmp = new File("employee.json");
        JSONArray items=null;
        try {
            JSONParser parser = new JSONParser();
            JSONObject object = (JSONObject) parser.parse(new FileReader(fileEmp));
            items=(JSONArray)object.get("emp");
            }
        catch (IOException ex) {
            System.out.println("ИО ЕКСЕПТИОН" + ex.getMessage());
           }
        catch (ParseException ex) {
            System.out.println("ПАРС ексептион" + ex.getMessage());
            }
            return items;
    }

    public static void main( String[] args )
    {
        ArrayList<Employee> emps = new ArrayList();
        EmpFactory emp = null;
        Employee staff = null;

        try {
            JSONArray items = App.toArray();
            for(Object i: items )
            {
                emp = new EmpFactory();
                staff = emp.createEmployee(((JSONObject)i).get("position").toString());
                staff.setHours(parseInt(((JSONObject)i).get("hours").toString()));
                staff.setId(parseInt(((JSONObject)i).get("id").toString()));
                staff.setName(((JSONObject)i).get("name").toString());
                staff.setPosition(((JSONObject)i).get("position").toString());
                staff.setProjects(((JSONObject) i).get("project").toString());
                staff.setSub();
                staff.healingPay();
                emps.add(staff);
            }

            System.out.printf("%-20s%-20s%-20s%-20s%-50s%-20s%-20s%n", "Сотрудник", "Должность", "Часов","Ставка","Людей в подчинении*бонус=за людей","за проекты","Зарплата");

            for (Employee v : emps) {
                System.out.printf("%-20s",v.getName());
                System.out.printf("%-20s",v.getPosition());
                System.out.printf("%-20s",v.getHours());
                System.out.printf("%-20s",v.getRate());
                System.out.printf("%-50s",v.healingPay()/v.BASE + "*" + v.BASE + "=" + v.healingPay());
                System.out.printf("%-20s",v.projectPay());
                System.out.printf("%-20s%n",v.buh());
                }

        }
        catch (Exception ex) {
            System.out.println("ИО ЕКСЕПТИОНн" + ex.getMessage());
        }

    }

}

