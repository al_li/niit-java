package StaffDemo;

public class Employee {
    private int id;
    private String name;
    private int worktime;
    private double payment;

    Employee(int new_id, String new_name) {
        this.id = new_id;
        this.name = new_name;
    }

    public String getName() {
        return name;
    }

    public int getWorktime() {
        return worktime;
    }

    public double getPayment() {
        return payment;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }

    public void setWorktime(int worktime) {
        this.worktime = worktime;
    }
}