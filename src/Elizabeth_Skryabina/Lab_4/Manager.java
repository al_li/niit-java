package StaffDemo;

public class Manager extends Employee implements Project {
    private String name_project;
    private double personal_contribution;
    int count_inferior;
    int head_const;
    private int money_project;

    Manager(int new_id, String new_name) {
        super(new_id, new_name);
    }

    public void setCount_inferior(int count_inferior) {
        this.count_inferior = count_inferior;
    }

    public void setMoney_project(int new_money_project) {
        this.money_project = new_money_project;
    }

    public void setPersonal_contribution(double personal_contribution) {
        this.personal_contribution = personal_contribution;
    }

    public String getName_project() {
        return name_project;
    }

    public void setName_project(String name_project) {
        this.name_project = name_project;
    }

    @Override
    public double pay_project(int money_project, double personal_contribution) {
        return money_project * personal_contribution;
    }

    void pay() {
        setPayment(pay_project(money_project, this.personal_contribution));
    }
}

class ProjectManager extends Manager implements Heading {
    ProjectManager(int new_id, String new_name, String New_name_project) {
        super(new_id, new_name);
        setName_project(New_name_project);
    }

    public void setBonus_pr_manager(double contribution_pr_manager, int HEAD_CONST_PR_MANAGER) {
        setPersonal_contribution(contribution_pr_manager);
        head_const = HEAD_CONST_PR_MANAGER;
    }

    @Override
    public int pay_heading(int count_programmer, int head_const) {
        return count_programmer * head_const;
    }

    void pay() {
        super.pay();
        setPayment(getPayment() + pay_heading(count_inferior, head_const));
    }
}

class SeniorManager extends ProjectManager {
    SeniorManager(int new_id, String new_name, String New_name_project) {
        super(new_id, new_name, New_name_project);
    }

    public void setBonus_senior(double contribution_senior, int HEAD_CONST_SENIOR) {
        setPersonal_contribution(contribution_senior);
        head_const = HEAD_CONST_SENIOR;
    }
}
