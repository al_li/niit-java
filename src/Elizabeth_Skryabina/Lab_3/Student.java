import java.math.BigDecimal;
import java.math.RoundingMode;

public class Student {
    private int ID; //- идентификационный номер
    private String FIO; //- фамилия и инициалы
    Group group; //- ссылка на группу (объект Group)
    int Marks[] = new int[15]; //- массив оценок
    int Num; //- количество оценок

    //создание студента с указанием ИД и ФИО
    public Student(int newID, String newFIO) {
        this.ID = newID;
        this.FIO = newFIO;
    }

    //зачисление в группу
    public void setGroup(Group str) {
        this.group = str;
    }

    public String getFIO() {
        return this.FIO;
    }

    public int getID() {
        return this.ID;
    }

    public Group getgroup() {
        return this.group;
    }

    //добавление оценки
    void AddMarks() {
        this.Marks[Num] = (2 + (int) (Math.random() * 4));
        this.Num++;
    }

    //вычисление средней оценки студента
    double CalculatingAverageMarkStudent() {
        int summ = 0;
        for (int i = 0; i < this.Num; i++)
            summ += this.Marks[i];
        double middle = (double) summ / Num;
        return new BigDecimal(middle).setScale(3, RoundingMode.UP).doubleValue();
    }

    void printStudent() {
        System.out.print(this.ID + " " + this.FIO + " - ");
        for (int i = 0; i < this.Num; i++)
            System.out.print(Marks[i] + " ");
        System.out.print(" (" + CalculatingAverageMarkStudent() + ")");
    }
}
