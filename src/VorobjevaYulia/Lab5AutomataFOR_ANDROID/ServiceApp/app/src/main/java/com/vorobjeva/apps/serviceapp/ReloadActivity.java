package com.vorobjeva.apps.serviceapp;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

public class ReloadActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {


    private View mProgressView;
    private TextView mStateText;

    private ServiceAsyncQueryHandler mServiceQueryHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reload);

        mProgressView = findViewById(R.id.progressLayout);
        mStateText = (TextView) findViewById(R.id.drinksStatusText);
        mStateText.setMovementMethod(new ScrollingMovementMethod());

        mServiceQueryHandler = new ServiceAsyncQueryHandler(getContentResolver(), new UpdateDataBaseListener() {
            @Override
            public void onUpdateCompleted() {
                getLoaderManager().restartLoader(0, null, ReloadActivity.this);
            }
        });

        mProgressView.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getLoaderManager().restartLoader(0, null, ReloadActivity.this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(
                this,
                DrinksContentProvider.CONTENT_URI,
                null,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        String dataString = DatabaseUtils.dumpCursorToString(data);
        mProgressView.setVisibility(View.GONE);
        mStateText.setText(dataString);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public void onButtonClick(View view) {
        mProgressView.setVisibility(View.VISIBLE);
        String uriString = null;

        switch (view.getId()) {
            case R.id.countReloadBtn:
                uriString = DrinksContentProvider.URL;
                break;
            case R.id.priceReloadBtn:
                uriString = DrinksContentProvider.URL + "/prices";
                break;
        }

        mServiceQueryHandler.startUpdate(0, null, Uri.parse(uriString), null, null, null);
    }
}
