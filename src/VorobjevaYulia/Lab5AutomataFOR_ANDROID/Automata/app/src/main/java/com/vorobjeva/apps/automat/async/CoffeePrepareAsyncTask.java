package com.vorobjeva.apps.automat.async;

import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

public class CoffeePrepareAsyncTask extends AsyncTask {

    private WeakReference<Context> mContextWeakReference;
    private WeakReference<OnCoffeePreparedListener> mListenerWeakReference;
    private int mPreparationTime;
    private String mName;

    public CoffeePrepareAsyncTask(Context context, OnCoffeePreparedListener listener, int preparationTime, String name) {
        mListenerWeakReference = new WeakReference<>(listener);
        mPreparationTime = preparationTime;
        mContextWeakReference = new WeakReference<>(context);
        mName = name;
    }

    @Override
    protected Object doInBackground(Object[] params) {
        try {
            TimeUnit.SECONDS.sleep(mPreparationTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Context context = mContextWeakReference.get();
        if (null != context) {
            Uri uri = Uri.parse("content://" + "com.vorobjeva.apps.serviceapp.drinksprovider" + "/drinks/" + mName.toLowerCase());
            context.getContentResolver().update(uri, new ContentValues(), null, null);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        OnCoffeePreparedListener listener = mListenerWeakReference.get();
        if (null == listener) {
            return;
        }
        listener.onCoffeePrepared(mName);
    }

}
