package com.vorobjeva.apps.automat.async;

public interface OnCoffeePreparedListener {
    void onCoffeePrepared(String name);
}
