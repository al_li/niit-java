package com.vorobjeva.apps.automat.model;

/**
 * Created by yuliavorobjeva on 26.04.17.
 */

public class AutomatButton {
    private String mText;
    private String mPrice;
    private boolean mIsEnabled;

    public AutomatButton(String text, String price) {
        mText = text;
        mPrice = price;
    }

    public String getTitle() {
        return mText;
    }

    public String getPrice() {
        return mPrice;
    }

    public boolean isEnabled() {
        return mIsEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.mIsEnabled = isEnabled;
    }


}
