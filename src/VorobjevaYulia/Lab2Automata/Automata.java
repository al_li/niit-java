/**
 * Created by yuliavorobjeva on 17.03.17.
 */
import java.util.Scanner;
import java.time.*;


public class Automata {
     enum STATES{
        OFF,WAIT,ACCEPT,CHECK,COOK
    }

    private int cash;
    private String[] menu = new String[5];
    private  int [] prices = new int[5];

    private STATES state;

    private int HumanChoice;
    private int HumanMoney;

    Automata(){
        cash = 0;
        menu[0] = "1 - Espresso";
        menu[1] = "2 - Latte";
        menu[2] = "3 - Chocolate";
        menu[3] = "4 - Tea";
        menu[4] = "5 - Milk";
        prices[0] = 35;
        prices[1] = 45;
        prices[2] = 40;
        prices[3] = 25;
        prices[4] = 30;
        state = STATES.OFF;
        HumanMoney = 0;
        HumanChoice = 0;

    }

    public void  ON(){
        if(state==STATES.OFF)
            state = STATES.WAIT;
    }

    public void  OFF(){
        if(state!=STATES.OFF&&state!=STATES.COOK)
            state = STATES.OFF;
    }

    public void printMenu(){
        for(int i=0;i<5;i++)
            System.out.println(menu[i]+" costs "+prices[i] + "rubles");
    }

    public void  printState(){
        switch (state){
            case OFF:
                System.out.println("Automat is off");
                break;
            case WAIT:
                System.out.println("Please, choose your drink");
                break;
            case ACCEPT:
                System.out.println("Please, enter your money");
                break;
            case CHECK:
                System.out.println("Please, wait. Automata is counting money:)");
                break;
            case COOK:
                System.out.println("Please,wait.Automata is doing your drink now");
                break;
        }

    }

    public void coin(int x){
        if(state==STATES.WAIT&&x>0) {
            HumanMoney += x;
            state=STATES.ACCEPT;
        }
    }
    public void choice(int x) {
        if (state == STATES.ACCEPT) {
            if (x >= 0 && x < 5) {
                HumanChoice = x;
                state = STATES.CHECK;
            }
        }
    }

    public void check(){
        if(state==STATES.CHECK){
            if(HumanMoney>=prices[HumanChoice])
                state=STATES.COOK;
        }
    }

    public void cook(){
        if(state==STATES.COOK)
        {
            state=STATES.WAIT;
            HumanMoney=0;
        }
    }

    public void cancel(){
        if(state!=STATES.COOK){
            state=STATES.WAIT;
        }
    }

    public STATES getState(){
        return state;
    }

    public void setState(STATES yourState){
        state = yourState;
    }

    public int getCash(){
        return cash;
    }

    public String[] getMenu(){
        return menu;
    }

    public int[] getPrices(){
        return prices;
    }

    public int getHumanChoice(){
        return HumanChoice;
    }

    public int getHumanMoney(){
        return HumanMoney;
    }


    public void setHumanMoney(int x){
        HumanMoney=x;
    }


}
