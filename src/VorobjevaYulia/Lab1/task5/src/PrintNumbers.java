import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by yuliavorobjeva on 09.03.17.
 */
public class PrintNumbers {
    public static void main(String[] args) {
        String ch = args[0];
        int x = Integer.parseInt(ch);
        int arrX[] = new int[10];
        String arrString[][] = new String[10][7];
        int i = 0,count = 0, j=0;
        String one[] = {
                "    * ",
                "   ** ",
                "  * * ",
                "    * ",
                "    * ",
                "    * ",
                "  ****"
        };

        String two[] ={
                "    **   ",
                "   *  *  ",
                "      *  ",
                "     *   ",
                "    *    ",
                "   *     ",
                "   ***** "
        };

        String three[] = {
                "    **   ",
                "   *  *  ",
                "      *  ",
                "     *   ",
                "      *  ",
                "   *  *  ",
                "    **   "
        };

        String four[] = {
                "       * ",
                "     * * ",
                "    *  * ",
                "   ******",
                "       * ",
                "       * ",
                "       * "
        };

        String five[] = {
                "  *****  ",
                "  *      ",
                "  *      ",
                "  *****  ",
                "      *  ",
                "      *  ",
                "  *****  "


        };

        String six [] =  {
                "   ****  ",
                "  *    * ",
                "  *      ",
                "  *****  ",
                "  *    * ",
                "  *    * ",
                "   ****  "
        };

        String seven [] = {
                "  ****** ",
                "       * ",
                "      *  ",
                "     *   ",
                "    *    ",
                "   *     ",
                "  *      "
        };

        String eight [] = {
                "    ***  ",
                "   *   * ",
                "   *   * ",
                "    ***  ",
                "   *   * ",
                "   *   * ",
                "    ***  "
        };

        String nine[] = {
                "    ***  ",
                "   *   * ",
                "   *   * ",
                "    **** ",
                "       * ",
                "   *   * ",
                "    ***  "
        };

        String zero [] = {
                "    ***  ",
                "   *   * ",
                "   *   * ",
                "   *   * ",
                "   *   * ",
                "   *   * ",
                "    ***  "
        };
        while(x>0){
            arrX[i]=x%10;
            x=x/10;
            //System.out.print(arrX[i]);
            i++;
        }
        for(i=0;i<arrX.length;i++)
        {
            switch (arrX[i]){
                case 1:
                    arrString[i] = one;
                    count++;
                    break;
                case 2:
                    arrString[i] = two;
                    count++;
                    break;
                case 3:
                    arrString[i] = three;
                    count++;
                    break;
                case 4:
                    arrString[i] = four;
                    count++;
                    break;
                case 5:
                    arrString[i] = five;
                    count++;
                    break;
                case 6:
                    arrString[i] = six;
                    count++;
                    break;
                case 7:
                    arrString[i] = seven;
                    count++;
                    break;
                case 8:
                    arrString[i] = eight;
                    count++;
                    break;
                case 9:
                    arrString[i] = nine;
                    count++;
                    break;
                case 0:
                    arrString[i] = zero;
                    break;
            }
        }
        //System.out.println("COUNT"+count);
        while(j <7) {
            for (i = count - 1; i >= 0; i--) {
                System.out.printf("%s", arrString[i][j]);
            }
            System.out.println("\n");
            j++;
        }

    }

    }

