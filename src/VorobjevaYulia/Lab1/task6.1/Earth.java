/**
 * Created by yuliavorobjeva on 13.03.17.
 */
 class Circle {
    private double Radius;
    private double Ference;//длина окружности
    private double Area;//площадь круга

    Circle() {
        Radius =0;
        Ference = 0;
        Area = 0;
    }
    void setRadius(double x){
        Radius = x;
        Ference = Math.PI*2*Radius;
        Area = Math.PI*Math.pow(Radius,2);
    }

    void setFerence(double x){
        Ference = x;
        Radius = (Ference/(2*Math.PI));
        Area = Math.PI*Math.pow(Radius,2);
    }

    void setArea(double x){
        Area = x;
        Radius=Math.sqrt((Area/Math.PI));
        Ference = 2*Math.PI*Radius;
    }

    double getRadius(){
        return Radius;
    }

    double getFerence(){
        return Ference;
    }

    double getArea(){
        return Area;
    }
}

public class Earth {
    public static void main(String[] args) {
        Circle myEarth = new Circle();
        myEarth.setRadius(6378.1);
        double myEarthFerence = myEarth.getFerence();
        double myEarthRadius = myEarth.getRadius();
        System.out.println(myEarthRadius);

        Circle myEarthwithRope = new Circle();
        myEarthwithRope.setFerence(myEarthFerence+0.001);
        double myEarthwithRopeRadius = myEarthwithRope.getRadius();
        System.out.println(myEarthwithRopeRadius);
        System.out.println("Зазор равен"+((myEarthwithRopeRadius-myEarthRadius)*1000)+"м");


    }


}
