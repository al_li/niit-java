/**
 * Created by yuliavorobjeva on 13.03.17.
 */

class Circle {
    private double Radius;
    private double Ference;//длина окружности
    private double Area;//площадь круга

    Circle() {
        Radius =0;
        Ference = 0;
        Area = 0;
    }
    void setRadius(double x){
        Radius = x;
        Ference = Math.PI*2*Radius;
        Area = Math.PI*Math.pow(Radius,2);
    }

    void setFerence(double x){
        Ference = x;
        Radius = (Ference/(2*Math.PI));
        Area = Math.PI*Math.pow(Radius,2);
    }

    void setArea(double x){
        Area = x;
        Radius=Math.sqrt((Area/Math.PI));
        Ference = 2*Math.PI*Radius;
    }

    double getRadius(){
        return Radius;
    }

    double getFerence(){
        return Ference;
    }

    double getArea(){
        return Area;
    }
}


public class Pool {
    public static void main(String[] args) {

        Circle myPool = new Circle();
        Circle myPoolWithPath = new Circle();
        myPool.setRadius(3);
        myPoolWithPath.setRadius(4);
        double myPoolWithPathArea = myPoolWithPath.getArea();
        double myPoolArea = myPool.getArea();
        double PathArea = myPoolWithPathArea-myPoolArea;
        double PathCost = PathArea*1000;
        double myPoolwithPathRadius = myPoolWithPath.getRadius();
        double FenceCost = myPoolwithPathRadius*2000;
        double generalCost = PathCost+FenceCost;
        System.out.printf("Your path and fense will cost %.2f roubles\n", generalCost);



    }
}
