import java.util.List;

/**
 * Created by yuliavorobjeva on 11.04.17.
 */
public class TeamLead extends Programmer implements Heading {

    @Override
    public double getHeadingPayment() {
        return 0.1 * payment * mSubordinatesCount;
    }

    @Override
    protected void calculateTotalPayment(){
        totalPayment = getProjectPayment() + getCountPayment() + getHeadingPayment();
    }
}