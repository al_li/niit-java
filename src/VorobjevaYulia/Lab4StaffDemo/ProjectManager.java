/**
 * Created by yuliavorobjeva on 11.04.17.
 */
public class ProjectManager extends Manager {

    @Override
    public double getHeadingPayment(){
        return payment * mSubordinatesCount;
    }

    protected void calculateTotalPayment() {
        totalPayment = getProjectPayment() + getHeadingPayment();
    }
}

