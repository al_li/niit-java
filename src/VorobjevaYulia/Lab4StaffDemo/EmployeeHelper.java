/**
 * Created by yuliavorobjeva on 20.04.17.
 */
import java.util.List;

public class EmployeeHelper {
    public static int calculateSubordinates(Employee sheff, List<Employee> people) {
        int count = 0;
        switch (sheff.getEmployeeType()) {
            case TEAMLEAD:
                for (Employee employee : people) {
                    if (employee instanceof Tester || employee instanceof Programmer) {
                        List<ProjectType> sheffsProjects = ((TeamLead)sheff).getProjectTypes();
                        List<ProjectType> subordinatesProjects = ((Engineer)employee).getProjectTypes();
                        for (ProjectType projectType : subordinatesProjects) {
                            if (sheffsProjects.contains(projectType)) {
                                count++;
                                break;
                            }
                        }
                    }
                }
                break;
            case PROJECT_MANAGER:
                for (Employee employee : people) {
                    if (employee instanceof Engineer) {
                        List<ProjectType> sheffsProjects = ((ProjectManager)sheff).getProjectTypes();
                        List<ProjectType> subordinatesProjects = ((Engineer)employee).getProjectTypes();
                        for (ProjectType projectType : subordinatesProjects) {
                            if (sheffsProjects.contains(projectType)) {
                                count++;
                                break;
                            }
                        }
                    }
                }
                break;
        }
        return count;
    }
}
