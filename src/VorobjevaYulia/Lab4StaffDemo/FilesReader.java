import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by yuliavorobjeva on 20.04.17.
 */
public class FilesReader {
    public  static String readFile() throws FileNotFoundException, IOException,Exception{
        BufferedReader bufferedReader;
        String AllEmployee = null;
        StringBuffer stringBuffer;
        try {
            bufferedReader = new BufferedReader(new FileReader("/Users/yuliavorobjeva/Desktop/Java/Staff 2/src/Employee.txt"));
            stringBuffer = new StringBuffer();
            while ((AllEmployee = bufferedReader.readLine()) != null) {
                stringBuffer.append(AllEmployee + "\n");
            }
            AllEmployee = stringBuffer.toString();
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return AllEmployee;
    }
}
