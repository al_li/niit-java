/**
 * Created by yuliavorobjeva on 11.04.17.
 */
public interface Project {
    public double getProjectPayment();
}
