/**
 * Created by yuliavorobjeva on 02.04.17.
 */
import java.io.*;

public class Decanat {

    private Student[] students;
    private Group[] groups;

    public static void main(String[] args)  throws  FileNotFoundException,
            IOException, Exception {
        Decanat myDecanat = new Decanat();
        int numberOfGroups = myDecanat.makeGroups();//читает файл с группами и создает массив групп
        int numberOfStudents = myDecanat.makeStudents();//читает файл, создает массив студентов,
        // присваивает каждому студенту id,фио, ссылку на группу и кол-во оценок
        myDecanat.FormGroupsWithStudents(numberOfStudents);//заполняет группы студентами
        myDecanat.addRandomMarks(numberOfStudents);//заполняет массив оценок рандомными числами от 1 до 5
        myDecanat.showAllInfo(numberOfStudents);
        myDecanat.writeStudentsToFile(numberOfStudents);
        myDecanat.writeGroupsToFile(numberOfGroups);
    }

    private void writeStudentsToFile(int numberOfStudents){
        try {
            FileWriter writer = new FileWriter("/Users/yuliavorobjeva/Desktop/NewStudents.txt");
            for(int i = 0;i<numberOfStudents;i++){
                writer.write(students[i].getID() +",  " + students[i].getFio() +",  " + students[i].getGroupTitle() + ", средний бал: " + students[i].findCum());
                if(students[i].getIsHead())
                    writer.write(" HEAD");
                writer.write("\n");
            }
            writer.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }



    }

    private void writeGroupsToFile(int numberOfGroups){
        try{
            FileWriter writer= new FileWriter("/Users/yuliavorobjeva/Desktop/NewGroups.txt");
            for(int i =0;i<numberOfGroups;i++){
                writer.write(groups[i].getTitle() + ", число студентов " + groups[i].getNum() + "\n");
            }
            writer.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }

    private void showAllInfo(int numberOfStudents){

        for(int i=0;i< numberOfStudents;i++){
            System.out.printf("Student's ID - %d, ", students[i].getID());
             System.out.printf("name - %s, " , students[i].getFio());
            System.out.printf("group - %s, ", students[i].getGroupTitle());
            students[i].showMarks();
            System.out.printf("Cum - %.1f ", students[i].findCum());
            if(students[i].getIsHead())
                System.out.printf(" IT IS HEAD");
            System.out.println("\n");
        }
    }
    private void changeHeadInGroup(Group group, Student Head, Student NotHead){
        group.changeHead(Head,NotHead);
    }

    private void ChangeGroupForSrudent(Student student, Group NewGroup){
        student.changeGroup(NewGroup);
    }

    private  void deleteBadStudents(int numberOfGroups, int numberOfStudents) {
        for (int i = 0; i < numberOfGroups; i++)
            groups[i].deleteBadStudents(numberOfStudents);
    }


    private void addRandomMarks(int numberOfStudents){
        for(int i=0;i<numberOfStudents;i++)
            students[i].setMarks();


    }

    private void FormGroupsWithStudents(int numberOfStudents){
        int j=0,k = 0, x =0;
        for (int i = 0; i < numberOfStudents; i++) {
            if (groups[0].getTitle().equals(students[i].getGroupTitle())) {
                groups[0].setStudent(students[i], j,1);
                j++;
            }
            if (groups[1].getTitle().equals(students[i].getGroupTitle())) {
                groups[1].setStudent(students[i],k,1);
                k++;
            }
            if (groups[2].getTitle().equals(students[i].getGroupTitle())) {
                groups[2].setStudent(students[i], x,1);
                x++;
            }


        }
    }
    private int makeStudents() throws Exception {//чтение файла со студентами и создание экземпляров класса
        String allStudents = readFileStudents();
        String allStudentsArray[] = allStudents.split("\n");
        int numberOfStudents= allStudentsArray.length;
        String arrayForEachStudent[][] = new String[numberOfStudents][];
        for(int i =0;i<numberOfStudents;i++){
            arrayForEachStudent[i] = allStudentsArray[i].split(",");
        }
        students = new Student[50];
        for(int i=0;i<numberOfStudents;i++){
            int id = Integer.parseInt(arrayForEachStudent[i][0]);
            String FIO = arrayForEachStudent[i][1];
            int num = Integer.parseInt(arrayForEachStudent[i][3]);
            if(arrayForEachStudent[i][2].equals(groups[0].getTitle())) {
                students[i] = new Student(id, FIO, num, groups[0]);
                //groups[0].plusNum(1);
            }
            if(arrayForEachStudent[i][2].equals(groups[1].getTitle())) {
                students[i] = new Student(id, FIO, num, groups[1]);
                //groups[1].plusNum(1);
            }
            if(arrayForEachStudent[i][2].equals(groups[2].getTitle())) {
                students[i] = new Student(id, FIO, num, groups[2]);
                //groups[2].plusNum(1);
            }
            if(arrayForEachStudent[i].length==5){
                students[i].setHead();
            }
        }
        return numberOfStudents;
    }


    private int makeGroups()throws Exception {//чтение файла с группами и создание экземпляров класса
        String allGroups = readFileGroups();
        String allGroupsArray[] = allGroups.split("\n");
        int numberOfGroups = allGroupsArray.length;
        groups = new Group[numberOfGroups];
        for (int i = 0; i < numberOfGroups; i++) {
            groups[i] = new Group(allGroupsArray[i]);
        }
        return numberOfGroups;
    }


    private String readFileGroups() throws FileNotFoundException,IOException,Exception{
        BufferedReader groups;
        String allGroups = null;
        StringBuffer stringBuffer;
        try{
            groups = new BufferedReader(new FileReader("/Users/yuliavorobjeva/Desktop/Group.txt"));
            stringBuffer = new StringBuffer();
            while ((allGroups = groups.readLine())!=null){
                stringBuffer.append(allGroups + "\n");
            }
            allGroups = stringBuffer.toString();
            groups.close();
        }
        catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
        return allGroups;
    }


    private String readFileStudents() throws FileNotFoundException,IOException,Exception{
        BufferedReader students;
        String AllStudents = null;
        StringBuffer stringBuffer;
        try {
            students = new BufferedReader(new FileReader("/Users/yuliavorobjeva/Desktop/students.txt"));
            stringBuffer = new StringBuffer();
            while ((AllStudents = students.readLine()) != null) {
                stringBuffer.append(AllStudents + "\n");
            }
            AllStudents = stringBuffer.toString();
            students.close();
        }
        catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
        catch (IOException ex) {
            System.out.println(ex);
        }
        return AllStudents;
    }
}
