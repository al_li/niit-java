/**
 * Created by yuliavorobjeva on 02.04.17.
 */

import java.math.*;

public class Student {
    private int ID;
    private String Fio;
    private Group group;
    private int num;
    private int [] Marks;
    private boolean isHead;

    public Student(int id, String FIO, int Num, Group thisgroup){
        ID = id;
        Fio = FIO;
        group = thisgroup;
        num = Num;
        isHead = false;
    }
    public void setHead(){
        this.isHead = true;
    }

    public void deleteHead(){//свержение старосты:)
        if (isHead)
            isHead = false;
    }



    public void setMarks(){//добавление оценок студенту
            Marks = new int[num];
        for(int i=0;i<num;i++){
            Marks[i] = (int)((Math.random()*5)+1);
        }
    }

    public void changeGroup(Group newGroup){//зачисление в группу или смена группы

        group = newGroup;
    }


    public Group getGroup(){
        return group;
    }

    public String getGroupTitle(){
        return group.getTitle();
    }

    public boolean getIsHead(){
        return isHead;
    }

    public void showMarks(){
        for(int i=0;i<num;i++)
            System.out.printf("%d, ", Marks[i]);
    }

    public double findCum() {//нахождение среднего балла студента
        double cum = 0;
        for (int i = 0; i < num; i++) {
            cum += Marks[i];
        }
        cum /= num;
        return cum;
    }

    public int getID(){
        return ID;
    }

    public String getFio(){
        return Fio;
    }

}
