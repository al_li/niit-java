package Network;

import java.io.IOException;
import java.net.URL;

public class ServerCurrency {
    //Double rate;
    String str;

    public ServerCurrency () throws IOException {

        String from = "USD";
        String to = "RUB";

        java.net.URL url = new java.net.URL(
                "http://www.webservicex.net/CurrencyConvertor.asmx"
                        + "/ConversionRate?FromCurrency=" + from
                        + "&ToCurrency=" + to);
        java.util.Scanner sc = new java.util.Scanner(url.openStream());

        // <?xml version="1.0" encoding="utf-8"?>
        sc.nextLine();

        // <double xmlns="http://www.webserviceX.NET/">0.724</double>
        str = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");

        sc.close();

        //rate = Double.parseDouble(str);
        //System.out.println(rate);
    }

    public String getCurrency() {return "USD"+"-RUB "+str;}
}