package Network;

import java.time.LocalTime;

public class ServerMessages {
    ServerMessages (){}

    // логин клиента который подключился к серверу
    private String login;
    //public String getLogin() {return login;}
    public void setLogin(String login) {this.login = login;}

    // массив логинов клиентов
    private String[] arrLogin = {"myLogin", "myLogin2", "myLogin3"};

    // массив сообщений
    private String[] arr_messages = {
                "Выбрать тему для выпускной работы",
                "Прочитать 10 главу ''Философия Java'' ",
                "Сделать рассылку сообщений",
                "Проверить почту",
                "Установить и настроить IntelliJ IDEA"};

    // массив времени отправки сообщения
    private String[] arr_time = {"9.00","11.00","13.00","15.00","17.00"};

    // получение сообщения
    public String getMessages() {

        // проверяем зарегистрирован ли у нас такой клиент
        boolean client_login = false;
        for (int i=0; i<arrLogin.length; i++ ) {
            if (arrLogin[i].equals(login)) {
                client_login = true;
                break;
            }
        }

        // узнаем текущее время
        LocalTime lt = LocalTime.now();
        long minute = lt.getMinute();
        long hours = lt.getHour();
        // проверяем в массиве времени отправки зарезервировано ли у нас текущее время?
        String real_time = hours + "." + minute;
        boolean time = false;
        for (String t : arr_time){
            if (t.equals(real_time)){
                time = true;
                break;
            }
        }

        // если клиент у нас зарегистрирован и текущее время для отправки сообщения подошло
        if (client_login && time) {
            String messages = null;
            // выбираем сообщение для текущего времени и отправляем его
            for (int i=0; i<arr_time.length; i++){
                if (arr_time[i].equals(real_time)){
                    messages = arr_messages[i];
                }
            }
            return messages;
        }else return "Для Вас нет сообщений";
    }
}