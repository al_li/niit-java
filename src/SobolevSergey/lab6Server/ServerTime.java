package Network;

import java.time.LocalTime;

public class ServerTime {
    public ServerTime (){}

    LocalTime lt = LocalTime.now();

    private long sec = lt.getSecond();
    private long minute = lt.getMinute();
    private long hours = lt.getHour();

    public long getSec() {return sec;}
    public long getMinute() {return minute;}
    public long getHours() {return hours;}
}