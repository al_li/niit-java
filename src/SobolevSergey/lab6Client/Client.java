package Network;

/*Лабораторный практикум №6

1 Задача 1.
Разработать сетевое приложение, в котором сервер по запросу клиента высылает значение даты/времени.
Клиент, построенный на основе GUI отображает дату и время в виде цифровых часов.

2 Задача 2.
Разработать сетевое приложение, сервер которого пересылает клиентам случайный афоризм из хранящегося
на сервере текстового файла.

3 Задача 3.
Разработать сетевое приложение, в котором сервер рассылает клиентам сообщения о событиях,
привязанных к определенному времени. Сообщения и клиенты связываются по ID.

4 Задача 4.
Разработать сетевое приложение, которое предоставляет пользователю возможность узнать курс валюты
относительно другой валюты.

*/

import java.io.*;
import java.net.*;
import java.time.LocalTime;

public class Client {
    static String fserver_time,fserver_aphorism,fserver_messages,fserver_currency;

    public static void main(String[] args) throws IOException {

        // запускаем графическую форму клиента (рабочий стол)
        new ClientGUI();

        System.out.println("Клиент стартовал");
        Socket server = null;

        if (args.length==0) {
            System.out.println("Использование: java Client hostname");
            System.exit(-1);
        }

        System.out.println("Соединяемся с сервером "+args[0]);

        server = new Socket(args[0],1234);
        BufferedReader in  = new BufferedReader( new  InputStreamReader(server.getInputStream()));
        PrintWriter out = new PrintWriter(server.getOutputStream(),true);
        BufferedReader inu = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
           try {
                Thread.sleep(100);
           } catch (InterruptedException e1) {
                e1.printStackTrace();
           }

            // 1. отправляем запрос на сервер с целью получить текущее время сервера
            out.println("time");
            fserver_time = in.readLine();

            // 2. отправляем запрос на сервер каждые 10 сек с целью получить афоризм
            LocalTime lt = LocalTime.now();
            long sec = lt.getSecond();
            if (sec%10 == 0) {
                out.println("aphorism");
                fserver_aphorism = in.readLine();
            }

            // 3. отправляем запрос (свой ID или login) на сервер с целью получить сообщения c событием
            out.println("myLogin");
            fserver_messages = in.readLine();

            // 4. отправляем запрос на сервер с целью получить курс валют
            out.println("currency");
            fserver_currency = in.readLine();

            // если с сервера приходит событие "exit" у клиента все отключается
            if (fserver_messages.equalsIgnoreCase("exit"))
                break;
        }

        out.close();
        in.close();
        inu.close();
        server.close();
    }

    // выводим в графическую форму результаты ответа с сервера
    public String print_time(){return fserver_time;}
    public String random_aphorism(){return fserver_aphorism;}
    public String event_messages(){return fserver_messages;}
    public String currency_rate(){return fserver_currency;}
}