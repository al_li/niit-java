package AnalogClock;
/*
Лабораторный практикум №5

Проект AnalogClock
Разработать версию стрелочных часов

*/

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.transform.Rotate;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalTime;

public class AnalogClock extends Application implements Runnable{
    public static void main(String[] args){
        launch(args);
    }

    public AnalogClock() throws FileNotFoundException {
        (new Thread(this)).start();
    }

    public void run(){
        while (true){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return;
            }
            Platform.runLater(() -> {
                repaint();
            });
        }
    }

    Image fon = new Image(new FileInputStream("src/main/java/AnalogClock/fon.png"));
    Image hour = new Image(new FileInputStream("src/main/java/AnalogClock/hour.png"));
    Image min = new Image(new FileInputStream("src/main/java/AnalogClock/minute.png"));
    Image second = new Image(new FileInputStream("src/main/java/AnalogClock/second.png"));

    private ImageView clock=new ImageView(fon);
    private Arrows hour_arrow=new Arrows(hour,0);
    private Arrows min_arrow=new Arrows(min,0);
    private Arrows sec_arrow=new Arrows(second,0);

    private Pane root=new Pane();

    @Override
    public void start(Stage primaryStage) throws Exception{
        root.getChildren().addAll(clock);
        Scene scene =new Scene(root,400,400);
        primaryStage.setTitle("Аналоговые часы в JavaFX");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void repaint(){
        LocalTime lt = LocalTime.now();

        long sec = lt.getSecond();
        long minute = lt.getMinute();
        long hours = lt.getHour();

        double sec_angle = 6*sec;
        double min_angle = (0.1)*(minute*60 + sec);
        double hour_angle = (0.5)*(hours*60 + minute);

        root.getChildren().remove(sec_arrow);

        sec_arrow=new Arrows(second,sec_angle);
        root.getChildren().addAll(sec_arrow);
        root.getChildren().remove(min_arrow);

        min_arrow=new Arrows(min,min_angle);
        root.getChildren().addAll(min_arrow);
        root.getChildren().remove(hour_arrow);

        hour_arrow=new Arrows(hour,hour_angle);
        root.getChildren().addAll(hour_arrow);
    }
}

class Arrows extends Pane {
    private ImageView imageView;

    public Arrows(Image image,double angle){
        this.imageView = new ImageView(image);
        imageView.setTranslateX(187);
        imageView.setTranslateY(0);

        Rotate rotate=new Rotate();
        rotate.setPivotX(13);
        rotate.setPivotY(200);
        rotate.setAngle(angle);
        imageView.getTransforms().addAll(rotate);
        getChildren().addAll(imageView);
    }
}