package staff;

// Pesonal - работник по найму с оплатой за фактически отработанное время. Имеет ставку за час.
public class Personal  extends Employee implements WorkTime {
    protected int base;

    public Personal(int id, String name, int base) {
        super(id, name);
        this.base = base;
    }

    public int workTime(int worktime,int base){
        return worktime*base;
    }

    public void calc(){
            setPayment(workTime(worktime,base));
    }
}

// Cleaner - уборщица.
final class Cleaner extends Personal {
    public Cleaner(int id, String name, int base) {
        super(id, name, base);
    }
}

// Driver - водитель.
final class Driver extends Personal{
    public Driver(int id, String name, int base) {
        super(id, name, base);
    }
}