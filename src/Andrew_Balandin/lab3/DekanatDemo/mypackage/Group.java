package mypackage;
/**
 * Created by me on 4/9/17.
 */


class Group
{
    public final static int numOfStudentsInGroup = 15;
    private String title;
    private Student students[];
    private Student head;

    Group(String title)
    {
        this.title = title;
        students = new Student[numOfStudentsInGroup];
        head = null;
    }

    Student getHead()
    {
        return head;
    }

    void takeStudent(Student student)// throws RuntimeException
    {
        for(int i = 0; i < students.length; i++)
            if(students[i] == null)
            {
                students[i] = student;
                return;
            }
        throw new RuntimeException("Group overflow!");
    }

    void removeStudent(Student student)
    {
        for(int i = 0;  i < students.length; i++)
        {
            if(students[i] == student)
            {
                if(student == head)
                    head = null;
                students[i] = null;
                return;
            }
        }
        throw new RuntimeException("Can not remove student!");
    }

    Student[] getStudents()
    {
        return students;
    }

    int getNumOfStudents()
    {
        int num = 0;
        for(Student st : students)
            if(st != null)
                num++;
        return num;
    }

    Student searchStudent(int id)
    {
        for(Student st : students)
            if(st != null && st.getID() == id)
                return st;
        return null;
    }

    Student searchStudent(String fio)
    {
        for(Student st : students)
            if(st != null && fio.equals(st.getFIO()))
                return st;
        return null;
    }

    void choiceHead(Student student)
    {
        this.head = student;
    }

    float calcAverageRate()
    {
        float sumRates = 0f;
        float numRates = 0f;
        for(Student st : students)
            if(st != null)
            {
                sumRates += st.calcAverageMark();
                numRates++;
            }
        return sumRates / numRates;
    }

    public String toString()
    {
        return this.title;
    }
}
