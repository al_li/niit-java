package mypackage;
/**
 * Created by me on 4/9/17.
 */

import org.json.simple.parser.*;
import org.json.simple.*;
import java.io.*;
import java.util.Random;

public class Dekanat
{
    public final static int numOfStudentsInDekanat = 50;
    public final static int numOfGroupsInDekanat = 5;
    private Student students[] = new Student[numOfStudentsInDekanat];
    private Group groups[] = new Group[numOfGroupsInDekanat];

    public Dekanat(String students_file, String groups_file)
    {
        try
        {
            FileReader fr = new FileReader(students_file);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(fr);
            JSONObject js = (JSONObject) obj;
            JSONArray items = (JSONArray) js.get("students");
            int j = 0;
            //new students
            for(Object i : items)
                students[j++] = new Student(
                        Integer.parseInt(((JSONObject) i).get("id").toString()),
                        ((JSONObject) i).get("fio").toString());
            fr = new FileReader(groups_file);
            parser = new JSONParser();
            obj = parser.parse(fr);
            js = (JSONObject) obj;
            items = (JSONArray) js.get("groups");
            j = 0;
            //new groups
            for (Object i : items)
                groups[j++] = new Group(((JSONObject) i).get("title").toString());
        }
        catch(FileNotFoundException ex)
        {
            System.out.println(ex.getMessage());
        }
        catch (ParseException ex)
        {
            System.out.println(ex.getMessage() + " Wrong input file!");
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    public Dekanat(String file)
    {
        this(file, file);
        try
        {
            FileReader fr = new FileReader(file);
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(fr);
            JSONObject js = (JSONObject) obj;
            JSONArray items = (JSONArray) js.get("students");
            //fill groups
            for (Object i : items)
            {
                Student tmpStudent = searchStudent( Integer.parseInt(((JSONObject) i).get("id").toString()));
                Group tmpGroup = searchGroup( (((JSONObject) i).get("group").toString()) );
                moveStudent(tmpStudent, tmpGroup);

            }
            items = (JSONArray) js.get("groups");
            int j = 0;
            //read heads
            for(Object i : items)
                if(groups[j] != null)
                {
                    Student student = groups[j].searchStudent(
                            Integer.parseInt( ((JSONObject) i).get("head_id").toString() ) );
                    groups[j].choiceHead(student);
                    j++;
                }
        }
        catch(FileNotFoundException ex)
        {
            System.out.println(ex.getMessage());
        }
        catch (ParseException ex)
        {
            System.out.println(ex.getMessage() + " Wrong input file!");
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    public void saveGroupsAndStudentsInFile(String file)
    {
        try
        {
            FileWriter fw = new FileWriter(file);
            JSONObject jsonObjForSaving = new JSONObject();
            JSONArray jsonGroups = new JSONArray();
            JSONArray jsonStudents = new JSONArray();

            for(Group g : groups)
            {
                if(g != null)
                {
                    JSONObject tmpG = new JSONObject();
                    tmpG.put("title", g.toString());
                    tmpG.put("head_id", g.getHead().getID());
                    jsonGroups.add(tmpG);
                    for(Student st : g.getStudents())
                    {
                        if(st != null)
                        {
                            JSONObject tmpS = new JSONObject();
                            tmpS.put("fio", st.getFIO());
                            tmpS.put("id", st.getID());
                            tmpS.put("group", g.toString());
                            jsonStudents.add(tmpS);
                        }
                    }
                }
            }

            jsonObjForSaving.put("groups", jsonGroups);
            jsonObjForSaving.put("students", jsonStudents);
            fw.write(jsonObjForSaving.toJSONString());
            fw.close();
        }
        catch (IOException ex)
        {
            System.out.println(ex.getMessage());
        }
    }

    public void randomMarks()
    {
        Random rnd = new Random();
        rnd.setSeed(47);
        for(Student st : students)
        {
            if(st != null)
                for(int j = 0; j < 10; j++)
                    st.addMark(rnd.nextInt(4) + 1);
        }
    }

    public String getStatistics()
    {
        String retVal = "";
        for(Group g : groups)
        {
            if(g != null)
            {
                retVal += "\nGROUP: " + g.toString() + "\n---\n" + "ID\t          FIO          \t AVG MARK" + "\n" +
                "-------------------------------------\n";
                for(Student s : g.getStudents())
                    if(s != null)
                        retVal += s.getID() + "\t " + String.format("%20s", s.getFIO()) + "\t " + s.calcAverageMark() + "\n"; // + s.getGroup().toString() + "\n";
                retVal += "---\ntotal: " + g.getNumOfStudents() + " student(s), average mark: " + g.calcAverageRate() + "\n";
                retVal += "HEAD: " + g.getHead().getID() + " " + g.getHead().getFIO() + "\n";
            }
        }
        //information about groups
        return retVal;
    }

    public void moveStudent(Student student , Group group)
    {
        if(student.getGroup() != null)
            student.getGroup().removeStudent(student);
        student.toGroup(group);
        group.takeStudent(student);
    }

    public void delStudentByMarks(float minMark)
    {
        for(int i = 0; i < students.length; i++)
        {

            if(students[i] != null && students[i].calcAverageMark() <= minMark)
            {
                if(students[i].getGroup() != null)
                    students[i].getGroup().removeStudent(students[i]);
                students[i] = null;
            }
        }
    }

    public void choiceHeadsInGroups()
    {
        int j;
        Random rnd = new Random();
        rnd.setSeed(System.nanoTime());
        for(Group g : groups)
        {
            if(g != null)
            {
                while(g.getHead() == null)
                {
                    j = rnd.nextInt(Group.numOfStudentsInGroup - 1);
                    if(g.getStudents()[j] != null)
                        g.choiceHead(g.getStudents()[j]);
                }
            }
        }
    }

    public void rndTransfer()
    {
        Random rnd = new Random();
        rnd.setSeed(System.nanoTime());
        int nGroup;
        int nStudent;
        while(true)
        {
            nGroup = rnd.nextInt(numOfGroupsInDekanat - 1);
            nStudent = rnd.nextInt(numOfStudentsInDekanat - 1);
            if(this.groups[nGroup] == null || this.students[nStudent] == null)
                continue;
            else if(this.groups[nGroup] != null && this.groups[nGroup].getNumOfStudents() < Group.numOfStudentsInGroup)
                break;
        }
        this.moveStudent(this.students[nStudent], this.groups[nGroup]);
    }

    public void fillGroups()
    {
        int k = 0;
        for(int i = 0; i < numOfGroupsInDekanat; i++)
            for(int j = 0; j < getNumOfStudents()/getNumOfGroups() + getNumOfStudents()%getNumOfGroups(); j++)
                if(this.groups[i] != null && this.students[k] != null)
                    this.moveStudent(this.students[k++], this.groups[i]);
    }

    Group searchGroup(String name)
    {
        for(Group g : groups)
            if(g != null && name.equals(g.toString()))
                return g;
        return null;
    }

    Student searchStudent(int id)
    {
        for(Student st : students)
            if(st != null && st.getID() == id)
                return st;
        return null;
    }

    private int getNumOfGroups()
    {
        int i = 0;
        for(Group g : groups)
            if(g != null)
                i++;
        return i;
    }

    private int getNumOfStudents()
    {
        int i = 0;
        for(Student st : students)
            if(st != null)
                i++;
        return i;
    }
}