package mypackage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by BalandinAS on 11.04.2017.
 */
public class StudentTest
{
    String fio = "Ivanov I.I.";
    String titleGroup = "g-1";
    int id = 25;
    Student[] studentsArray = new Student[Group.numOfStudentsInGroup];
    Student student = new Student(id, fio);
    Group group = new Group(titleGroup);

    @Before
    public void before()
    {
        student.toGroup(group);
    }

    @Test
    public void toGroupTest() throws Exception
    {
        assertEquals(group, student.getGroup());
    }

    @Test
    public void getIDTest() throws Exception
    {
        assertEquals(25, student.getID());
    }

    @Test
    public void getFIOTest() throws Exception
    {
        assertEquals(fio, student.getFIO());
    }

    @Test
    public void getGroupTest() throws Exception
    {
        assertEquals(group, student.getGroup());
    }

    @Test
    public void addMarkTest() throws Exception
    {
        for(int i = 0; i < 10; i++)
            student.addMark(5);
        assertEquals(5, (int)student.calcAverageMark());
    }

    @Test
    public void calcAverageMarkTest() throws Exception
    {
        for(int i = 0; i < 10; i++)
            student.addMark(i + 1);
        assertEquals(5.5, student.calcAverageMark(), 0.001);
    }
}