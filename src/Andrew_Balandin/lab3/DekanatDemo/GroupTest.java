package mypackage;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by BalandinAS on 11.04.2017.
 */
public class GroupTest
{
    String fio1 = "Ivanov I.I.";
    int id1 = 25;
    String fio2 = "Petroff I.I.";
    int id2 = 26;
    String titleGroup = "g-1";

    Student[] studentsArray = new Student[Group.numOfStudentsInGroup];
    Student student1 = new Student(id1, fio1);
    Student student2 = new Student(id2, fio2);
    Group group = new Group(titleGroup);

    @Before
    public void before()
    {
        group.takeStudent(student1);
        group.takeStudent(student2);
    }


    @Test
    public void getHeadTest() throws Exception
    {
        group.choiceHead(student1);
        assertEquals(student1, group.getHead());
    }

    @Test
    public void takeStudentTest() throws Exception
    {
        studentsArray[0] = student1;
        studentsArray[1] = student2;
        assertArrayEquals(studentsArray, group.getStudents());

        Student students [] = new Student[17];
        for(int i = 0; i < 17; i++)
            students[i] = new Student(i + id1, fio1);
        try
        {
        for(int i = 0; i < 17; i++)
            group.takeStudent(students[i]);
        fail("Group.takeStudent() test exception failed!");
        }
        catch (RuntimeException e)
        {
            assertEquals("Group overflow!", e.getMessage());
        }
    }

    @Test
    public void removeStudentTest() throws Exception
    {
        studentsArray[0] = student1;
        //studentsArray[1] = student2;
        group.removeStudent(student2);
        assertArrayEquals(studentsArray, group.getStudents());
        try
        {
            group.removeStudent(student2);
            fail("Group.removeStudent() test exception failed!");
        }
        catch (RuntimeException e)
        {
            assertEquals("Can not remove student!", e.getMessage());
        }
    }

    @Test
    public void getStudentsTest() throws Exception
    {
        studentsArray[0] = student1;
        studentsArray[1] = student2;
        assertArrayEquals(studentsArray, group.getStudents());
    }

    @Test
    public void getNumOfStudentsTest() throws Exception
    {
        assertEquals(2, group.getNumOfStudents());
    }

    @Test
    public void searchStudentTest() throws Exception
    {
        assertEquals(student1, group.searchStudent(25));
    }

    @Test
    public void searchStudent1Test() throws Exception
    {
        assertEquals(student1, group.searchStudent(fio1));

    }

    @Test
    public void choiceHeadTest() throws Exception
    {
        group.choiceHead(student1);

    }

    @Test
    public void calcAverageRateTest() throws Exception
    {
        group.choiceHead(student2);
        assertEquals(student2, group.getHead());
    }

    @Test
    public void toStringTest() throws Exception
    {
        assertEquals(titleGroup, group.toString());
    }

}