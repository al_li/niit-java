import mypackage.Dekanat;

/**
 * Created by me on 4/9/17.
 */
public class DekanatDemo
{
    public static void main(String[] args)
    {
        Dekanat dekanat = new Dekanat(
                "students.json",
                "groups.json");

        System.out.println("\n***Exams***");
        dekanat.fillGroups();
        dekanat.choiceHeadsInGroups();
        dekanat.randomMarks();
        System.out.print(dekanat.getStatistics());

        System.out.println("\n***After Exams***");
        dekanat.delStudentByMarks(2.5f);
        dekanat.choiceHeadsInGroups();
        System.out.print(dekanat.getStatistics());

        System.out.println("\n***After Holidays***");
        for(int i = 0; i < 10; i++)
            dekanat.rndTransfer();
        dekanat.choiceHeadsInGroups();
        System.out.print(dekanat.getStatistics());

        System.out.println("\n***After Save/Open***");
        dekanat.saveGroupsAndStudentsInFile("students_in_groups.json");
        Dekanat dekanat1 = new Dekanat("students_in_groups.json", "students_in_groups.json");
        dekanat1.fillGroups();
        dekanat1.choiceHeadsInGroups();
        //dekanat1.randomMarks();
        System.out.print(dekanat1.getStatistics());

        System.out.println("\n***After Save/Open Groups & Students***");
        Dekanat dekanat2 = new Dekanat("students_in_groups.json");
        System.out.print(dekanat2.getStatistics());
    }
}