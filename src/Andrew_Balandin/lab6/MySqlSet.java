import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

class SqlSet
{
    public Integer id;
    public String message;
    public Integer hour;
    public Integer minute;
}

public class MySqlSet
{
    ArrayList<SqlSet> list = new ArrayList<>();

    /*public static void main(String[] args)
    {
        new MySqlSet(2);
    }*/

    public MySqlSet(Integer id)
    {
        MySqlConnect mysqlConnect = new MySqlConnect();
        String sql = "SELECT * FROM messages WHERE id = " + id + ";";
        //String sql = "SELECT * FROM `messages`";
        try {
            PreparedStatement statement =
                    mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next())
            {
                SqlSet sqlSet = new SqlSet();
                sqlSet.id = Integer.parseInt(rs.getString("id"));
                sqlSet.message = rs.getString("message_txt");
                sqlSet.hour = Integer.parseInt(rs.getString("message_time").split(":")[0]);
                sqlSet.minute = Integer.parseInt(rs.getString("message_time").split(":")[1]);
                list.add(sqlSet);
                /*System.out.println("id : " + sqlSet.id);
                System.out.println("message : " + sqlSet.message);
                System.out.println("time : " + sqlSet.hour);
                System.out.println("time : " + sqlSet.minute);*/
            }
            System.out.println("load messages:");
            list.stream().forEach(e -> System.out.println(e.id + " " + e.message + " " + e.hour +":" + e.minute));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            mysqlConnect.disconnect();
        }
    }
}

