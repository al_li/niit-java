public class CurrencyConverter
{
    //Double rate;
    String str;

    public CurrencyConverter ()
    {

        String from = "USD";
        String to = "RUB";
        try
        {

            java.net.URL url = new java.net.URL(
                    "http://www.webservicex.net/CurrencyConvertor.asmx"
                            + "/ConversionRate?FromCurrency=" + from
                                + "&ToCurrency=" + to);
            java.util.Scanner sc = new java.util.Scanner(url.openStream());

            // <?xml version="1.0" encoding="utf-8"?>
            sc.nextLine();

            // <double xmlns="http://www.webserviceX.NET/">0.724</double>
            str = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");
            System.out.println("str:" + str);
            sc.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    //public String getCurrency() {return "USD"+"-RUB "+str;}
    public String getCurrency() {return str;}
}