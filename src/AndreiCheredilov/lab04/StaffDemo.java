package Andrei;

public class StaffDemo {
    public static void main(String[] args) {
        Employee obj = new Employee();
        obj.employeesCreating();

        for(int i =0; i < obj.numOfEmpEmployees; i++) {
            obj.employees[i].setWorktime(160);
            obj.employees[i].salaryCalculator();
            obj.employees[i].printOfResult();
        }
    }
}
