package Andrei;
// расчет оплаты исходя из руководства (количество подчиненных).
public interface IHeading {
    double calcHeadingPayment(int sizeOfTeam,int rateOfLeading);
}
