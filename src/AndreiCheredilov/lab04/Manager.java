package Andrei;

public class Manager extends Employee implements IProject {

    protected int projectBudget;
    protected double projectContribution;
    String project;

    Manager(int id, String name, String project, int projectBudget, double projectContribution){
        super(id, name);
        this.project = project;
        this.projectBudget = projectBudget;
        this.projectContribution = projectContribution;
    }

    @Override
    public double calcProjectPayment(int projectBudget, double projectContribution) {
        return projectBudget*projectContribution;
    }

    public void salaryCalculator() {
        setPayment(calcProjectPayment(projectBudget, projectContribution));
    }
}
