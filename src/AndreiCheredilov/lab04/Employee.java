package Andrei;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.Collections;

//работник. Основной родительский класс для всех разновидностей работников.
public class Employee {

    private int id;
    private String name;
    protected int worktime;
    private int base = 0;
    private int rateOfLeading = base * 24;
    private double payment = 0;
    private String project = null;
    private String position;
    protected Employee[] employees = new Employee[16];
    protected Project[] projects = new Project[100];
    protected int numOfEngineer = 0;
    protected int numOfTeamLeader = 0;
    protected int numOfPersonal = 0;
    protected int numOfManager = 0;
    protected int numOfEmpEmployees = 0;
    protected int sizeOfTeam;


    public Employee() {
    }

    public Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWorktime() {
        return worktime;
    }

    public void setWorktime(int worktime) {
        this.worktime = worktime;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public double getPayment() {
        return payment;
    }

    public void setPayment(double payment) {
        this.payment = payment;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void salaryCalculator() {
    }
    public void setSizeOfTeam(int sizeOfTeam) {
        this.sizeOfTeam = sizeOfTeam;
    }
    public int getSizeOfTeam() {
        return sizeOfTeam;
    }




    public void employeesCreating() {

        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File("input.xml"));
            doc.getDocumentElement().normalize();
            NodeList listOfEmployees = doc.getElementsByTagName("employee");

            for (int s = 0; s < 16; s++) {

                Node firstPersonNode = listOfEmployees.item(s);
                if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element firstPersonElement = (Element) firstPersonNode;

                    NodeList idList = firstPersonElement.getElementsByTagName("ID");
                    Element idElement = (Element) idList.item(0);

                    NodeList idFNList = idElement.getChildNodes();
                    id = Integer.parseInt((idFNList.item(0).getNodeValue().trim()));

                    NodeList fioList = firstPersonElement.getElementsByTagName("name");
                    Element fioElement = (Element) fioList.item(0);
                    NodeList fioFNList = fioElement.getChildNodes();

                    name = (fioFNList.item(0).getNodeValue().trim());

                    NodeList positionList = firstPersonElement.getElementsByTagName("position");
                    Element positionElement = (Element) positionList.item(0);
                    NodeList positionFNList = positionElement.getChildNodes();

                    position = (positionFNList.item(0).getNodeValue().trim());

                    NodeList baseList = firstPersonElement.getElementsByTagName("base");
                    Element baseElement = (Element) baseList.item(0);
                    NodeList baseFNList = baseElement.getChildNodes();

                    base = Integer.parseInt((baseFNList.item(0).getNodeValue().trim()));
                    NodeList projectList = firstPersonElement.getElementsByTagName("project");
                    Element projectElement = (Element) projectList.item(0);
                    NodeList projectFNList = projectElement.getChildNodes();

                    project = (projectFNList.item(0).getNodeValue().trim());
                    if (s == 0) {
                        projects[0] = new Project(name, ((int) (1000000 + Math.random() * 2000000)));
                    }

                    if (position.equals("Programmer")) {
                        numOfEngineer++;
                        employees[s] = new Programmer(id, name, base, (projects[0].getName()),
                                (projects[0].getProjectBudget()), projects[0].getRateForProgrammer());
                        employees[s].setPosition(position);
                    } else if (position.equals("Tester")) {
                        numOfEngineer++;
                        employees[s] = new Tester(id, name, base, (projects[0].getName()),
                                (projects[0].getProjectBudget()), projects[0].getRateForTester());
                        employees[s].setPosition(position);
                    } else if (position.equals("Cleaner")) {
                        numOfPersonal++;
                        employees[s] = new Cleaner(id, name, base);
                        employees[s].setPosition(position);
                    } else if (position.equals("Driver")) {
                        numOfPersonal++;
                        employees[s] = new Driver(id, name, base);
                        employees[s].setPosition(position);
                    } else if (position.equals("ProjectManager")) {
                        numOfManager++;
                        employees[s] = new ProjectManager(id, name, (projects[0].getName()),
                                (projects[0].getProjectBudget()), projects[0].getForProjectManager(),
                                0, rateOfLeading);
                        employees[s].setPosition(position);
                    } else if (position.equals("TeamLeader")) {
                        numOfTeamLeader++;
                        employees[s] = new TeamLeader(id, name, base, (projects[0].getName()),
                                (projects[0].getProjectBudget()), projects[0].getRateForTeamLeader(),
                                0, rateOfLeading);
                        employees[s].setPosition(position);
                    }


                    if (position.equals("SeniorManager")) {
                        employees[s] = new SeniorManager(id, name, (projects[0].getName()),
                                (projects[0].getProjectBudget()),
                                projects[0].getRateForSeniorManager(), (employees.length - 1), rateOfLeading);
                        employees[s].setPosition(position);
                    }

                }
                numOfEmpEmployees++;
            }

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }

    }


    public void printOfResult() {
        System.out.println(id + " Employee: " + name + " position: " + position + " paycheck:  " + payment);
    }
}