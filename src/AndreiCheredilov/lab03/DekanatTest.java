import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DekanatTest {

    @Test
    public void studentsCreating() {
        boolean thrown = false;
        try {
            Dekanat obj = new Dekanat();
            obj.studentsCreating();
        } catch (Exception e) {
            thrown = true;
        }
        assertFalse(thrown);
        Student st = new Student(12313, "Bla Bla bla");
        assertEquals(st.getFio(), "Bla Bla bla");
        assertEquals(st.getID(), 12313);
    }


    @Test
    public void groupsCreating() {
        boolean thrown = false;
        try {
            Dekanat obj = new Dekanat();
            obj.studentsCreating();
            obj.groupsCreating();
        } catch (Exception e) {
            thrown = true;
        }
        assertFalse(thrown);
    }


    @org.junit.Test
    public void addRandomMarks() throws Exception {
        Dekanat dekanat = new Dekanat();
        dekanat.addRandomMarks();
    }
}