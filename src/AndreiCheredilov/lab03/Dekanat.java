import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Dekanat {

    protected final Student[] students = new Student[30];
    protected final Group[] groups = new Group[3];

    public Dekanat() {
    }

    public void studentsCreating() {

        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File("src/students.xml"));
            doc.getDocumentElement().normalize();
            NodeList listOfStudents = doc.getElementsByTagName("students");


            for (int s = 0; s < listOfStudents.getLength(); s++) {

                Node firstPersonNode = listOfStudents.item(s);
                if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element firstPersonElement = (Element) firstPersonNode;

                    NodeList idList = firstPersonElement.getElementsByTagName("id");
                    Element idElement = (Element) idList.item(0);
                    NodeList idFNList = idElement.getChildNodes();
                    int ID = Integer.parseInt((idFNList.item(0).getNodeValue().trim()));

                    NodeList fioList = firstPersonElement.getElementsByTagName("fio");
                    Element fioElement = (Element) fioList.item(0);
                    NodeList fioFNList = fioElement.getChildNodes();

                    String Fio = (fioFNList.item(0).getNodeValue().trim());
                    students[s] = new Student(ID, Fio);

                }
            }

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void groupsCreating() {

        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document doc = docBuilder.parse(new File("src/groups.xml"));
            doc.getDocumentElement().normalize();
            NodeList listOfStudents = doc.getElementsByTagName("groups");


            for (int s = 0; s < listOfStudents.getLength(); s++) {

                Node firstPersonNode = listOfStudents.item(s);
                if (firstPersonNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element firstPersonElement = (Element) firstPersonNode;

                    NodeList idList = firstPersonElement.getElementsByTagName("title");
                    Element idElement = (Element) idList.item(0);
                    NodeList idFNList = idElement.getChildNodes();
                    String Title = (idFNList.item(0).getNodeValue().trim());
                    groups[s] = new Group(Title);

                }
            }

        } catch (SAXParseException err) {
            System.out.println("** Parsing error" + ", line " + err.getLineNumber() + ", uri " + err.getSystemId());
            System.out.println(" " + err.getMessage());

        } catch (SAXException e) {
            Exception x = e.getException();
            ((x == null) ? e : x).printStackTrace();

        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void addRandomMarks() {
        for (int i = 0; i < (students.length); i++) {
            for (int j = 0; j < 10; j++) {
                (students[i]).addMarks((int) (Math.random() * 5 + 1));
            }
        }
    }

    public void placementOfStudents() {
        int[] arr = new int[students.length];
        ArrayList<Integer> ArrayList = new ArrayList<>(students.length + 1);
        for (int i = 0; i < students.length; i++) {
            ArrayList.add(i);
        }
        Collections.shuffle(ArrayList);
        for (int i = 0; i < ArrayList.size(); i++) {
            arr[i] = ArrayList.get(i);
        }
        int count = 0;
        for (Group gr : groups) {
            for (int j = 0; j < (students.length) / (groups.length); j++) {
                gr.addStudent(students[arr[count]]);
                (students[arr[count]]).setGroup(gr);
                count++;
            }
        }
    }

    public void choosingOfHeads() {
        for (Group gr : groups)
            gr.electionOfHead();
    }


    public void statisticsStorage()

    {
        double medianGradeOfStudent = 0.0;
        double medianGradeOfGroup = 0.0;

        for (Student st : students) {
            medianGradeOfStudent = st.averageGrade();
        }
        for (Group gr : groups) {
            medianGradeOfGroup = gr.medianGrade();
        }
    }

    public void randTransfer() {
        String Fio = (students[(int) (Math.random() * students.length)]).getFio();
        String Title = (groups[(int) (Math.random() * groups.length)]).getTitle();
        transferOfStudent(Fio, Title);

    }

    public void transferOfStudent(String Fio, String Title) {
        int temp = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].getFio().equals(Fio)) {
                for (int j = 0; j < groups.length; j++) {
                    if (groups[j].equals(students[i].getGroup())) {
                        groups[j].removingOfStudent(students[i]);
                        temp = i;
                    }
                }
                for (int j = 0; j < groups.length; j++) {
                    if (groups[j].getTitle().equals(Title)) {
                        students[i].setGroup(groups[j]);
                        groups[j].addStudent(students[temp]);
                    }
                }
            }
        }
    }

    public void expellingOfStudent() {
        double pass = 2.5;
        int count = 0;
        for (int i = 0; i < students.length; i++) {
            if (students[i].averageGrade() < pass) {
                students[i].removeGroup();
            }

        }

    }


    public void createXML() {
        try {

            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            Element root = doc.createElement("Dekanat");
            doc.appendChild(root);

            for (Group gr : groups) {
                String id = gr.getTitle();
                String averageGrade = (gr.medianGradeToString()).substring(0, 3);

                Element mainChild = doc.createElement("group");
                mainChild.setAttribute("name", id);
                root.appendChild(mainChild);

                Element mainChild2 = doc.createElement("medianGradeInGroup");
                String data = (Double.toString(gr.medianGrade())).substring(0, 3);
                mainChild2.appendChild(doc.createTextNode(data));
                mainChild.appendChild(mainChild2);


                for (Student st : students) {
                    if (st.getFio() != null && st.getGroup() != null) {
                        if ((st.getGroup().equals(gr))) {
                            String fio = st.getFio();
                            Element subChild = doc.createElement("student");
                            subChild.setAttribute("Fio", fio);

                            Element subSubChild = doc.createElement("Marks");
                            subSubChild.appendChild(doc.createTextNode(st.getMarks()));

                            Element subSubChild2 = doc.createElement("medianMark");
                            subSubChild2.appendChild(doc.createTextNode(Double.toString(st.averageGrade())));

                            Element subSubChild3 = doc.createElement("ID");
                            subSubChild3.appendChild(doc.createTextNode(Integer.toString(st.getID())));

                            Element subSubChild4 = doc.createElement("currentGroup");
                            subSubChild4.appendChild(doc.createTextNode(st.getGroup().getTitle()));

                            subChild.appendChild(subSubChild3);
                            subChild.appendChild(subSubChild2);
                            subChild.appendChild(subSubChild);
                            subChild.appendChild(subSubChild4);
                            mainChild.appendChild(subChild);
                        }
                    }
                }

            }
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
            String xmlString = sw.toString();

            File f = new File("output.xml");
            FileWriter fw = new FileWriter(f);
            fw.write(xmlString);
            fw.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void outputOfData() {
        try {
            File file = new File("output.xml");
            FileReader reader = new FileReader(file);
            BufferedReader reader2 = new BufferedReader(reader);
            String line;
            while ((line = reader2.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
        } catch (IOException er) {
            System.out.println(er.getMessage());
        }
    }

    public Student getStudent(int i) {
        return (students[i]);
    }
}

