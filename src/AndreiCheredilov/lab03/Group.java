public class Group {

    private String Title; //Название группы
    protected final Student[] Students = new Student[10];//Массив из ссылок на студентов
    private int Num = 0; //Количество студентов в группе
    private Student Head; //Ссылка на старосту (из членов группы)

    Group(String Title) {
        this.Title = Title;
        this.Num = 0;
    }

    public void setNewGroup(String Title) {
        this.Title = Title;
    }

    public String getTitle() {
        return Title;
    }

    public void addStudent(Student student) throws ArrayIndexOutOfBoundsException {
        if(Num < 10) {
            Students[Num] = student;
            Num++;
        }
    }

    public void electionOfHead() {
        double maxgrade = 0.0;
        int max = 100000;
        for (int i = 0; i < Students.length; i++) {
            if ((Students[i]).averageGrade() >= maxgrade) {
                maxgrade = (Students[i]).averageGrade();
                max = i;
            }
            Head = Students[max];
        }
    }

    public Student poiskpoFio(String Fio) {
        Student student = null;
        for (int i = 0; i < Num; i++) {
            if (Students[i].getFio().equals(Fio)) {
                student = Students[i];
            }
        }
        return student;
    }

    public Student poiskpoID(int ID) {
        Student student = null;
        for (int i = 0; i < Num; i++) {
            if ((Students[i]).getID() == ID)
                student = Students[i];
        }
        return student;
    }

    public double medianGrade() {
        double temp = 0.0;
        for (int i = 0; i < Num; i++) {
            if(Students[i] != null)
            temp += (Students[i]).averageGrade();
        }
        return (temp) / (Num);
    }

    public String medianGradeToString() {
        double temp = 0.0;
        for (int i = 0; i < Num && Students[i] != null ; i++) {
            temp += (Students[i]).averageGrade();
        }
        return Double.toString((temp) / (Num));
    }

    public void expelling(Student student) {
        for (int i = 0; i < Num; i++) {
            if ((Students[i]).equals(student))
                Students[i] = null;
        }
        throw new RuntimeException("Student cant be removed!");
    }

    public void printOfStudents() {
        for (Student st : this.Students)
            System.out.println(st.getFio());
    }

    public void removingOfStudent(Student student) {
        for (int i = 0; i < Students.length; i++) {
            if (student.equals(Students[i])) {
                if (Head.equals(Students[i])) {
                    Head = null;
                }
                    Students[i] = null;
                Num--;
            }
        }
    }
    public String getNum() {
        return Integer.toString(Num);
    }
}