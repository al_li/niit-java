public class Svertka {
    public static void main(String[] args) {
        int i = 0;
        int b = 0;
        int e = 0;
        char array[] = args[0].toCharArray();
        int len = array.length;

        while (i < len) {
            if (Character.isDigit(array[i])) {
                b = Character.digit(array[i], 10);
                break;
            }
            i++;
        }
        i = len - 1;
        while (i > 0) {
            if (Character.isDigit(array[i])) {
                e = Character.digit(array[i], 10);
                if (Character.isDigit(array[i - 1]))
                    e = (Character.digit(array[i - 1], 10)) * 10 + e;
                break;
            }
            i--;
        }
        i = b;

        while (i < e) {
            System.out.print(i++ + ",");
            if (i == e) {
                System.out.print(i);
            }
        }
    }
}

