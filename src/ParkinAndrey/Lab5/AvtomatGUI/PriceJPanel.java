import java.awt.*;
import java.util.*;
import javax.swing.*;

public class PriceJPanel extends JPanel{
	private int d_price;
	private Dimension dim;
	private int x_dist, y_dist, radius;
	
	public PriceJPanel(int _d_price){
		this.d_price = _d_price;
		
		this.setMinimumSize( new Dimension(50, 50) );
		this.setPreferredSize( new Dimension(150, 150) );
		this.setMaximumSize( new Dimension(200, 200) );
	}
	
	public void paintComponent(Graphics g){
		this.dim = this.getSize();
		x_dist = dim.width/2;
		y_dist = dim.height/2;
		radius = Math.min(x_dist,y_dist)-10;
		
		g.setColor(new java.awt.Color(139, 69, 19));
		g.fillOval(x_dist - radius, y_dist - radius, radius*2, radius*2);
		
		g.setColor(java.awt.Color.WHITE);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 24));
		int x_offset = 1;
		
		while(Math.floor(d_price/Math.pow(10,x_offset)) > 0) {
			x_offset++;
		}
		x_offset++;
		g.drawString(""+d_price, x_dist - x_offset*5, y_dist - 10);
		g.drawString("rub", x_dist - 3*5, y_dist + 20);
	}
}
