public class Drink{
	public String name;
	public int price;
	public int weight;
	
	Drink(String _name, int _price, int _weight){
		this.name   = _name;
		this.price  = _price;
		this.weight = _weight;
	}
}