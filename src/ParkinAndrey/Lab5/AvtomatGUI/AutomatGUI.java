import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.util.*;
import java.io.*;

interface AutomatGUIInterface{
    public void showText(String msg);
}

class AutomatGUI implements AutomatGUIInterface{
	private Automat aut;
	
	private JFrame autWindow;
	private Container container;
	private JPanel panel, panel_text, panel_menu, panel_cache;
	private Map<Integer, LabelJPanel> drink_menu;
	private Map<Integer, PriceJPanel> cache_menu;
	private LabelJPanel chancel_btn;
	private Integer selected_menu;
	
	private JTextArea textArea;
	
	public AutomatGUI() {
		this.aut = new Automat("menu.txt", this);
		this.drink_menu = new HashMap<Integer, LabelJPanel>();
		this.cache_menu = new HashMap<Integer, PriceJPanel>();
	}
	
	public void showText(String msg)
    {
        textArea.setForeground(Color.white);
		textArea.setText(msg);
		panel_text.repaint();
    }
	
	public void checkCook() {
		new javax.swing.Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if( aut.CheckCookFinish() ) {
					aut.Finish();
					selected_menu = null;
					((javax.swing.Timer)ae.getSource()).stop();
				}
			}
		}).start();
	}
	
	class MenuChoiceListener implements MouseListener {
		public void mouseClicked(MouseEvent e) {
			if( selected_menu == null ) {
				if( e.getSource() == chancel_btn ) {
					aut.Cancel(true);
				} else {
					for (Map.Entry<Integer, LabelJPanel> entry : drink_menu.entrySet()) {
						if( e.getSource() == entry.getValue() ) {
							selected_menu = entry.getKey();
							if( aut.Choice( selected_menu ) ) {
								aut.Cook();
								checkCook();
							} else {
								selected_menu  = null;
							}
						}
					}
				}
			}
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	
	class CacheListener implements MouseListener {
		public void mouseClicked(MouseEvent e) {
			if( selected_menu == null ) {
				for (Map.Entry<Integer, PriceJPanel> entry : cache_menu.entrySet()) {
					if( e.getSource() == entry.getValue() ) {
						aut.Coin( entry.getKey() );
					}
				}
			}
		}
		public void mousePressed(MouseEvent e) {}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}

	public void draw(){		
		int menu_len = this.aut.GetMenuLen() + 1; // + chancel button
		
		int[] caches = {1, 2, 5, 10, 20, 50, 100};
		int caches_len = caches.length;
	
		this.autWindow = new JFrame("Coffee & Tea Machine");
		this.autWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.autWindow.setSize(1000,900);
		
		this.panel = new JPanel();
        this.panel.setLayout(new BorderLayout());
          
        this.panel_text = new JPanel();
        this.panel_text.setLayout(new GridLayout(1,1));
		
		int menu_rows = (int)Math.floor(menu_len/3);
        this.panel_menu = new JPanel();
        this.panel_menu.setLayout(new GridLayout( menu_rows,3 ));
		
        this.panel_cache = new JPanel();
        this.panel_cache.setLayout(new GridLayout(caches_len,1));

		this.textArea = new JTextArea(3, 700);
        this.textArea.setLineWrap(true);
        this.textArea.setWrapStyleWord(true);
		Font font = new Font("Verdana", Font.BOLD, 15);
		this.textArea.setFont(font);
		this.textArea.setBackground(new java.awt.Color(139, 69, 19));
		this.textArea.setMargin(new Insets(10, 5, 5, 5));
		this.panel_text.add(textArea);
			
		for(int i=0; i<menu_len-1; i++){
			LabelJPanel l_panel = new LabelJPanel(aut.GetMenuItemLabel(i), aut.GetMenuItemPrice(i), aut.GetMenuItemWeight(i), false);
			l_panel.addMouseListener(new MenuChoiceListener());
			this.drink_menu.put(i, l_panel);
			this.panel_menu.add(l_panel);
		}
		this.chancel_btn = new LabelJPanel("Return money", 0, 0, true);
		this.chancel_btn.addMouseListener(new MenuChoiceListener());
		panel_menu.add(this.chancel_btn);
		
		for(int cache: caches){
			PriceJPanel p_panel = new PriceJPanel(cache);
			p_panel.addMouseListener(new CacheListener());
			this.cache_menu.put(cache, p_panel);
			this.panel_cache.add(p_panel);
		}
	
        this.panel.add(panel_text, BorderLayout.NORTH );
        this.panel.add(panel_menu, BorderLayout.CENTER );
        this.panel.add(panel_cache, BorderLayout.EAST  );
		
		this.container = this.autWindow.getContentPane();
		this.container.add(this.panel);

		this.autWindow.setVisible(true);

		this.aut.On();
	}
}