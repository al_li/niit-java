class ProjectManager extends Manager implements Heading {
	int base_for_employee;
	
	public ProjectManager( int id, String name, StaffProject project, int base_for_employee ) {
		super(id, name, project);
		this.base_for_employee = base_for_employee;
	}

	public int calculateHeadingSalary() {
		return project.GetEmployeesNumber() * this.base_for_employee;
	}
	
	public void calculateSalary() {
		super.calculateSalary();
		this.payment += this.calculateHeadingSalary();
	}
	
	protected String getPerconalInfo() {
		return "position: ProjectManager, " + super.getPerconalInfo();
	}
}