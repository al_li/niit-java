class StaffProject {
	private String name;
	private int budget;
	private int employees_number;
	
	public StaffProject( String name, int budget ) {
		this.name   = name;
		this.budget = budget;
		this.employees_number = 0;
	}
	
	public void AddEmployee() {
		this.employees_number++;
	}
	
	public void DeleteEmployee() {
		this.employees_number--;
	}
	
	public String GetName() {
		return name;
	}
	
	public int GetBudget() {
		return budget;
	}
	
	public int GetEmployeesNumber() {
		return employees_number;
	}
	
	public void Print() {
		System.out.println(this.name + " : budget = " + this.budget + ", employees = " + this.employees_number);
	}
}