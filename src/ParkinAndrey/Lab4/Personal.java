class Personal extends Employee implements WorkTime {
	int base;

	public Personal( int id, String name, int base ) {
		super(id, name);
		this.base = base;
	}
	
	public int calculateWorkTimeSalary() {
		return this.worktime * this.base;
	}
	
	public void calculateSalary() {
		this.payment = this.calculateWorkTimeSalary();
	}
	
	protected String getPerconalInfo() {
		return "-";
	}
}
