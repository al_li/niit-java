class Tester extends Engineer {
	public Tester( int id, String name, int base, StaffProject project ) {
		super(id, name, base, project);
	}
	
	protected String getPerconalInfo() {
		return "position: Tester, " + super.getPerconalInfo();
	}
}