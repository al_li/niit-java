abstract class Employee {
	protected int id;
	protected String name;
	protected int worktime;
	protected int payment;
	
	public Employee( int id, String name ) {
		this.id = id;
		this.name = name;
		this.worktime = 0;
		this.payment  = 0;
	}
	
	public abstract void calculateSalary();
	
	protected abstract String getPerconalInfo();
	
	public void setWorktime( int worktime ) {
		this.worktime = worktime;
	}
	
	public void Print() {
		System.out.println(this.name + " (" + this.getPerconalInfo() + ") ; worktime = " + this.worktime + " ; payment = " + this.payment); 
	}
}