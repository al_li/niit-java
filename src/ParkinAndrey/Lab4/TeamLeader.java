class TeamLeader extends Programmer implements Heading {
	int base_for_employee;
	
	public TeamLeader( int id, String name, int base, StaffProject project, int base_for_employee ) {
		super(id, name, base, project);
		this.base_for_employee = base_for_employee;
	}

	public int calculateHeadingSalary() {
		return project.GetEmployeesNumber() * this.base_for_employee;
	}
	
	public void calculateSalary() {
		super.calculateSalary();
		this.payment += this.calculateHeadingSalary();
	}
	
	protected String getPerconalInfo() {
		return "position: TeamLeader, project: " + project.GetName();
	}
}