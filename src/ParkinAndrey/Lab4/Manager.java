class Manager  extends Employee implements Project {
	protected StaffProject project;

	public Manager( int id, String name, StaffProject project ) {
		super(id, name);
		this.project = project;
	}

	public int calculateProjectSalary() {
		return project.GetBudget() / project.GetEmployeesNumber();
	}
	
	public void calculateSalary() {
		this.payment = this.calculateProjectSalary();
	}
	
	protected String getPerconalInfo() {
		return "project: " + project.GetName();
	}
}
