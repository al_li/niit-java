public class DigitalRangesZ1 {
	public static int digital_count;
	public static void PrintDigit(int digit){
		if (digital_count != 0)
			System.out.print(",");
		System.out.print(digit);
		digital_count++;
	}
	public static void main(String[] args){
		String ranges = args[0];
		digital_count = 0;
		for (String retval : ranges.split(",")){
			int del_position = retval.indexOf("-");
			if (del_position == -1)
				PrintDigit(Integer.parseInt(retval));
			else {
				for( int i=Integer.parseInt(retval.substring(0,del_position)); 
				i <= Integer.parseInt(retval.substring(del_position+1)); i++)
					PrintDigit(i);
			}
		}
	}
}