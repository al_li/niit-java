public class Collatz 
{
	public static int start_val = 1;
	public static long stop_val = 1000000;
	public static long collatz_num = 0;
	public static void collatz(long n) {
		collatz_num++;
		if (n == 1) return;
		else if (n % 2 == 0) collatz(n / 2);
		else collatz(3*n + 1);
	}
	public static void main(String[] args) {
		long MaxCollatzNum = 0;
		long MaxCollatzI = 0;
		for (long i = start_val; i < stop_val; i++) {
			collatz_num = 0;
			collatz(i);
			if(collatz_num > MaxCollatzNum) {
				MaxCollatzNum = collatz_num;
				MaxCollatzI = i;
			}
		}
		System.out.println("MaxCollatzNum = " + MaxCollatzNum + ": MaxCollatzI = " + MaxCollatzI);
	}
}