package ExchangeRate;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ExchangeRate extends JFrame {
   
   public ExchangeRate(){
      setTitle("Exchange Rate");
      setLayout(new FlowLayout());
      setSize(500,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      setLocationRelativeTo(null);
      
      JButton rateButton = new JButton("Get USD/RUB Rate");
      JLabel resultTextLabel = new JLabel("Exchange rate result:");
      final JLabel resultText = new JLabel("...");
      add(rateButton);
      add(resultTextLabel);
      add(resultText);
      setResizable(false);
      setVisible(true);
      
      rateButton.addActionListener(new ActionListener(){
         @Override
         public void actionPerformed(ActionEvent arg0) {
            java.net.URL url = null;
            String from = "USD";
            String to = "RUB";
            String rateDate = "0:0";
            
            try {
               url = new java.net.URL(
                  "http://currencyconverter.kowabunga.net/converter.asmx"
                               + "/GetConversionRate?CurrencyFrom=" + from
                               + "&CurrencyTo=" + to
                               + "&RateDate=" + rateDate);
            } catch (MalformedURLException e) {
               e.printStackTrace();
            }
            
            InputStream stream = null;
            try {
               stream = url.openStream();
            } catch (IOException e) {
               e.printStackTrace();
            }
            
            java.util.Scanner sc = new java.util.Scanner(stream);

            // <?xml version="1.0" encoding="utf-8"?>
            sc.nextLine();

            // <double xmlns="http://www.webserviceX.NET/">0.724</double>
            String str = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");

            sc.close();

            Double rate = Double.parseDouble(str);
            resultText.setText(rate.toString());
         }
      });
   }
   public static void main(String[] args) {
      System.out.println("Hello!");
      ExchangeRate rate = new ExchangeRate();
   }
}

