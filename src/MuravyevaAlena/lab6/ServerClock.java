import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ServerClock implements Runnable {
   static private Socket connection;
   static private ObjectOutputStream output;
   static private ObjectInputStream input;
   static private ServerSocket server;
   GregorianCalendar calendar= new GregorianCalendar();
   File aphorisms=new File("aforizm.txt");
   
   final int max = 10; 
   int rnd=0;
   int countString=0;
   int today;
   int month;
   int year;
   int hour;
   int minutes;
   int second;
   
   @Override
   public void run() {
      try {
         server=new ServerSocket(2503,100);
         while(true){
            connection= server.accept();
            output=new ObjectOutputStream(connection.getOutputStream());
            input=new ObjectInputStream(connection.getInputStream());
            calendar.setTime(new Date());
            today=calendar.get(Calendar.DAY_OF_MONTH);
            month=calendar.get(Calendar.MONTH)+1;
            year=calendar.get(Calendar.YEAR);
            hour=calendar.get(Calendar.HOUR);
            minutes=calendar.get(Calendar.MINUTE);
            second=calendar.get(Calendar.SECOND);
            String question=(String)input.readObject();
            if(question.compareTo("Show time")==0){
                 output.writeObject("Date " + today+ ":"+ month+ ":" + year +"\n"+
                  "Time " + hour+ ":"+ minutes+ ":" + second);
            }
         }
      }
      catch (HeadlessException e){
         e.printStackTrace();
      } 
      catch (UnknownHostException e){
         e.printStackTrace();
      } 
      catch (IOException e){
         e.printStackTrace();
      } catch (ClassNotFoundException e){
         e.printStackTrace();
      }
      finally {
         try {
            server.close();
            connection.close();
         } catch (IOException e) {
            e.printStackTrace();
         }
      }
   }
}
