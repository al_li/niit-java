
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import java.net.Socket;
import java.net.UnknownHostException;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;

public class ClientAphorism extends JFrame implements Runnable{
   static private Socket connection;
   static private ObjectOutputStream output;
   static private ObjectInputStream input;
   JTextField aphorismsText =new JTextField(50);
   
   public static void main(String[] args) {
      new Thread (new ClientAphorism ()).start();
      new Thread (new ServerAphorism()).start();
   }
   public ClientAphorism(){
      setTitle("Client-Server aphorisms");
      setLayout(new FlowLayout());
      setSize(700,300);
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
      JButton aphorismsButton= new JButton("Show aphorisms");
      add(aphorismsButton);
      aphorismsText.setEditable(false);
      add(aphorismsText);
      setLocationRelativeTo(null);
      setResizable(false);
      setVisible(true);
      aphorismsButton.addActionListener(new ActionListener(){
        
      @Override
      public void actionPerformed(ActionEvent arg0) {
            sendDate("Show aphorisms");
         }
      });
      }

   @Override
   public void run() {
      try {
         while(true){
            connection= new Socket(InetAddress.getByName("127.0.0.1"),2505);
            output=new ObjectOutputStream(connection.getOutputStream());
            input=new ObjectInputStream(connection.getInputStream());
            aphorismsText.setText((String)input.readObject());
            input.close();
            output.close();
            connection.close();
         }
      }
      catch (HeadlessException e) {
            e.printStackTrace();
      } 
      catch (ClassNotFoundException e) {
            e.printStackTrace();
      }
       catch (UnknownHostException e) {
         e.printStackTrace();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }
   
   private static void sendDate (Object obj){
         try {
            output.flush();
            output.writeObject(obj);
         } catch (IOException e) {
            e.printStackTrace();
         }
   }
}
