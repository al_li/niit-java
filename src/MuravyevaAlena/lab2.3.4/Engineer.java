package staffDemo;

class Engineer extends Employee implements Project{
   StaffDemo astaffDemo=new StaffDemo();
   public double bonus=0;
   
   public Engineer(int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public Engineer(double apayment,double abonus){
      super(apayment);
      bonus=abonus;
   }
   public double payment(double _worktime, int _base) {
      return base*(astaffDemo.getNormalTime()/getWorktime());
   }
   public void bonus(int budgetProject, double coefficient){
      bonus = budgetProject*coefficient;
   }
   @Override
   public void calculationPayment(){
      payment= payment(worktime,base)+bonus;
   }
}
class Programmer extends Engineer{
   public Programmer (int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public Programmer(double apayment,double abonus){
      super(apayment,abonus);
   }
}
class Tester extends Engineer{
   public Tester(int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public Tester(double apayment,double abonus){
      super(apayment,abonus);
   }
}
class TeamLeader extends Engineer implements Heading{
   private final double supplementForPeople=5000;
   private double headPay=0;
   
   public TeamLeader(int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public TeamLeader(double apayment,double abonus){
      super(apayment,abonus);
   }
   public void paymentForPeople(int counterPeople) {
      headPay= supplementForPeople*counterPeople;
   }
   @Override
   public void calculationPayment(){
      payment= payment(worktime,base)+headPay+bonus;
   }
}