package staffDemo;

class Personal extends Employee{
   StaffDemo astaffDemo=new StaffDemo();
   public Personal(int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public Personal(double apayment){
      super(apayment);
   }
}
class Cleaner extends Personal{
   public Cleaner(int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public Cleaner(double apayment){
      super(apayment);
   }
}
class Driver  extends Personal{
   public Driver(int aid,String aname,String aposition,int aworktime,int abase){
      super(aid,aname,aposition,aworktime,abase);
   }
   public Driver(double apayment){
      super(apayment);
   }
}
