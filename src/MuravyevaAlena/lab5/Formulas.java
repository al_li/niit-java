package sample;

public class Formulas{
    public int makeDegreeSecond(int second)
    {
        return second * 360/60;
    }
    public int makeDegreeMinutes(int minutes, int second)
    {
        return second * 360/60/60 + minutes*360/60;
    }
    public int makeDegreeHours(int hours, int minutes)
    {
        hours=hours%12;
        return hours * 360/12 + minutes*360/12/60;
    }
}
