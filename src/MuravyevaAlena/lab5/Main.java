package sample;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.Node;
import java.time.LocalTime;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

public class Main extends Application  {
    @Override
    public void start (Stage primaryStage)throws Exception {
        Formulas fm=new Formulas();
        StackPane root = new StackPane();
        Scene myScene=new Scene(root,500,600);
        primaryStage.setTitle("Smile Clock");
        primaryStage.setScene(myScene);

        Image clock = new Image(getClass().getResourceAsStream("cats.jpg"));
        Image secondsHand= new Image (getClass().getResourceAsStream("secStr.png"));
        Image minutesHand= new Image (getClass().getResourceAsStream("min2.png"));
        Image hoursHand= new Image (getClass().getResourceAsStream("houStr.png"));
        Image circle= new Image (getClass().getResourceAsStream("crug.png"));

        ImageView imageView = new ImageView(clock);
        Node nodeSecondsArrows = new ImageView(secondsHand);
        Node nodeMinutesArrows = new ImageView(minutesHand);
        Node nodeHoursArrows = new ImageView(hoursHand);
        Node nodeHoursCircle = new ImageView(circle);

        root.getChildren().add(imageView);
        root.getChildren().add(nodeSecondsArrows);
        root.getChildren().add(nodeMinutesArrows);
        root.getChildren().add(nodeHoursArrows);
        root.getChildren().add(nodeHoursCircle);


        root.getChildren().get(1).setTranslateY(secondsHand.getHeight()/2 -65);
        root.getChildren().get(2).setTranslateY(minutesHand.getHeight()/2 -minutesHand.getWidth()/2);
        root.getChildren().get(3).setTranslateY(hoursHand.getHeight()/2-20);
        root.getChildren().get(1).setTranslateY(secondsHand.getHeight()/2 -65);
        root.getChildren().get(4).setTranslateY(circle.getHeight()/2 -15);

        imageView.setX(60) ;
        imageView.setY(70) ;
        imageView.setFitHeight(450);
        imageView.setFitWidth(450);
        primaryStage.centerOnScreen();
        imageView.setPreserveRatio(true);
        primaryStage.show();
        LocalTime locTime= LocalTime.now();

        int startAngleSecond=fm.makeDegreeSecond(locTime.getSecond());
        int startAngleMinutes=fm.makeDegreeMinutes(locTime.getMinute(),locTime.getSecond())+180;
        int startAngleHours=fm.makeDegreeHours(locTime.getHour(),locTime.getMinute())+180;


        Rotate rotateSecond= new Rotate (startAngleSecond,secondsHand.getWidth()/2,65);
        Rotate rotateMinutes= new Rotate (startAngleMinutes,minutesHand.getWidth()/2,minutesHand.getWidth()/2);
        Rotate rotateHours= new Rotate (startAngleHours,hoursHand.getWidth()/2,20);

        nodeSecondsArrows.getTransforms().add (rotateSecond);
        nodeMinutesArrows.getTransforms().add (rotateMinutes);
        nodeHoursArrows.getTransforms().add (rotateHours);

        Timeline ltSecond= new Timeline();
        ltSecond.getKeyFrames().addAll(
                new KeyFrame(
                        Duration.millis (60000),
                        new KeyValue(rotateSecond.angleProperty(),startAngleSecond +360)
                )
        );

        Timeline ltMinutes= new Timeline();
        ltMinutes.getKeyFrames().addAll(
                new KeyFrame(
                        Duration.seconds (60*60),
                        new KeyValue(rotateMinutes.angleProperty(),startAngleMinutes +360)
                )
        );

        Timeline ltHours= new Timeline();
        ltHours.getKeyFrames().addAll(
                new KeyFrame(
                        Duration.seconds (60*60*12),
                        new KeyValue(rotateHours.angleProperty(),startAngleHours +360)
                )
        );

        ltSecond.setCycleCount(Animation.INDEFINITE);
        ltSecond.play();

        ltMinutes.setCycleCount(Animation.INDEFINITE);
        ltMinutes.play();

        ltHours.setCycleCount(Animation.INDEFINITE);
        ltHours.play();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
