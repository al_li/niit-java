import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({Driver.class, Programmer.class, Tester.class, TeamLeader.class, ProjectManager.class,SeniorManager.class})
public class Employee {

    enum POSITIONS {
        Cleaner, Driver, Programmer, Tester, Team_Leader, Project_Manager, Senior_Manager
    }

    @XmlElement
    private String id; //
    @XmlElement
    private String name; // имя
    @XmlElement
    private POSITIONS position; // должность

    private int workTime;   // отработанное время

    private int payment; // заработная плата

    public Employee() {
    }

    public Employee(String id, String name, POSITIONS position) {
        this.id = id;
        this.name = name;
        this.position = position;
    }

    public int getWorkTime() {
        return workTime;
    }

    public void setWorkTime(int workTime) {
        this.workTime = workTime;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public POSITIONS getPosition() {
        return position;
    }

    public String[] fieldsToStringArray() {
        String[] strArr = new String[10];
        return strArr;
    }
}
