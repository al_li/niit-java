import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "org")
public class StaffWrapper {

    private ArrayList<Employee> employees = new ArrayList<>(); // массив всех сотрудников
    private ArrayList<DevProject> project = new ArrayList<>(); // массив всех проектов

    @XmlElementWrapper(name = "projects")
    @XmlElement (name = "project")
    public ArrayList<DevProject> getProject() {
        return project;
    }

    public void setProject(ArrayList<DevProject> project) {
        this.project = project;
    }
    @XmlElementWrapper(name = "staff")
    @XmlElement(name = "employee")
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(ArrayList<Employee> employees) {
        this.employees = employees;
    }
}
