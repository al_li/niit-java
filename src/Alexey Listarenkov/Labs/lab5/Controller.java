import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ToggleButton;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.ArrayList;

public class Controller {

    Automata automata;

    @FXML
    private ToggleButton btnOn;
    @FXML
    private Label lblMonitor;
    @FXML
    private Button btn0;
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    @FXML
    private Button btn5;
    @FXML
    private Button btn6;
    @FXML
    private Button btn7;
    @FXML
    private Button btn8;
    @FXML
    private Button btn9;
    @FXML
    private Button coin5;
    @FXML
    private Button coin10;
    @FXML
    private Button coin50;
    @FXML
    private Button coin100;
    @FXML
    public ProgressBar pb;

    private ArrayList<Button> buttons = new ArrayList<>();
    private Stage stage;

    public void on(ActionEvent event) {
        if (btnOn.isSelected()) {
            automata = new Automata();
            System.out.println("автомат включен");
            printMessage();
            System.out.println(btn0.getId());
            initButtons();
        } else {
            automata.off();
            System.out.println("автомат отключен");
            clearButtons();
            printMessage();
        }
    }

    private void printMessage() {
        lblMonitor.setText(automata.getMessage());
        System.out.println(automata.getMessage());
    }

    private void initButtons() {
        buttons.add(btn0);
        buttons.add(btn1);
        buttons.add(btn2);
        buttons.add(btn3);
        buttons.add(btn4);
        buttons.add(btn5);
        buttons.add(btn6);
        buttons.add(btn7);
        buttons.add(btn8);
        buttons.add(btn9);

        for (int i = 0; i < automata.getMenu().length; i++) {
            buttons.get(i).setText(automata.getMenu()[i] + "\n" + automata.getPrice()[i]);
            buttons.get(i).setTextAlignment(TextAlignment.CENTER);
        }
    }

    private void clearButtons() {
        for (Button btn : buttons) {
            btn.setText("");
        }
        buttons.clear();
    }

    public void coin(ActionEvent event) {
        if (btnOn.isSelected()) {
            Button btn = (Button) event.getSource();
            String btnId = btn.getId();
            int coin = Integer.parseInt(btnId.substring(4));
            automata.coin(coin);
            printMessage();
        }
    }

    public void cancel(ActionEvent event) {
        if (btnOn.isSelected()) {
            automata.cancel();
            printMessage();
        }
    }

    public void choice(ActionEvent event) throws InterruptedException {
        if (btnOn.isSelected()) {
            // определение id нажатой кнопки
            Button btn = (Button) event.getSource();
            String btnId = btn.getId();
            int id = Integer.parseInt(btnId.substring(3));
            // если нажата кнопка, соответствющая какому либо напитку и меню
            if (id < automata.getMenu().length) {
                // если внесённая сумма достаточна для покупки
                if (automata.choice(automata.getMenu()[id])) {
                    printMessage(); // " Заберите сдачу "
                    automata.coin(0);
                    // начать готовить
                    automata.cook();

                    printMessage(); //" Приготовление напитка "

                    pb.progressProperty().bind(automata.task.progressProperty());
                    new Thread(automata.task).start();
                    automata.finish();
                    printMessage();
                }
            }
        }
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
