/*Реализовать алгоритм вычисления квадратного корня. 
Взять за основу пример программы. 
Изменить пример так, чтобы была возможность регулирования точности расчетов.
*/
class Sqrt
{
   double delta=0.00000001;
   double arg;

   Sqrt(double arg, int d) {
      this.arg=arg;
      this.delta=delta*d;
   }
   double average(double x,double y) {
      return (x+y)/2.0;
   }
   boolean good(double guess,double x) {
      return Math.abs(guess*guess-x)<delta;
   }
   double improve(double guess,double x) {
      return average(guess,x/guess);
   }
   double iter(double guess, double x) {
      if(good(guess,x))
         return guess;
      else
         return iter(improve(guess,x),x);
   }
   public double calc(double k) {
      return iter((1.0*k),arg);
   }
}

class Program
{
   public static void main(String[] args)
   {
      double val=Double.parseDouble(args[0]);
      
      
      for (int i = 1; i <= 100000000 ; i = i*10) {
         for (int k = 1;k <= 100 ; k= k*10 ) {
            Sqrt sqrt=new Sqrt(val, i);
            double result=sqrt.calc(0.001*k);
            System.out.println("Sqrt of "+val+"="+result+". Delta = "+sqrt.delta+" K = " + 0.001 * k);
            System.out.println();
         }
      }
   }
}