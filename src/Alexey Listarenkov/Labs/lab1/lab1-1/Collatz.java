/*
* Задача 1 Последовательность Коллатца
*
*Найти наибольшую последовательность Коллатца для чисел в диапазоне от 1 до 1 000 000.
*/

public class Collatz {

    static long argsLength;
    static long lengthCounter = 0;

    public static void collatz(long n) {
        //System.out.print(n + " ");
        
        if (n == 1){ 
            argsLength ++;
            return;
        }
        else if (n % 2 == 0) {
            argsLength++;
            collatz(n / 2);
        }
        else {
            argsLength++;
            collatz(3*n + 1);
        }
        
        
    }

    public static void main(String[] args) {
        //int i = Integer.parseInt(args[0]);
        for (int i = 1; i <= 1_000_000 ; i++){
          argsLength = 0;
          collatz(i);  
          //System.out.println("argsLenght = " + argsLenght);
          if (argsLength > lengthCounter) lengthCounter = argsLength;
        }
        
       System.out.println("maxLength = " + lengthCounter);
    }
}