import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlType(propOrder = {"id", "fio","group", "marks", "averageScore"})
public class Student {
    private String id;// идентификационный номер
    private String fio;// фамилия имя отчество
    @XmlElement (name = "group")
    private String group;// группа
    private ArrayList<Integer> marks; //массив оценок
    @XmlElement
    private double averageScore; // средний балл


    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getId() {
        return id;
    }
    //@XmlElement
    public void setId(String id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group.getTitle();
    }

    public ArrayList<Integer> getMarks() {
        return marks;
    }

    @XmlList
    public void setMarks(ArrayList<Integer> marks) {
        this.marks = marks;
    }

    public double getAverageScore() {
        return averageScore;
    }
    //@XmlElement
    public void setAverageScore() {
        int sum = 0;
        for (int m : marks) {
            sum += m;
        }
        this.averageScore = (double) sum / marks.size();
    }

}
