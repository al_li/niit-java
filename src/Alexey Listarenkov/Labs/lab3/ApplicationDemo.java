import javax.xml.bind.JAXBException;
import java.io.*;

public class ApplicationDemo {
    public static String[] st = {"Быбина Алена Евгеньевна", "Тернова Анна Андреевна", "Растригина Ангелина Ивановна", "Дегтярева Елена Вадимовна",
            "Цветкова Светлана Евгеньевна", "Майков Денис Васильевич", "Кабатова Алина Викторовна", "Хохлов Данил Олегович", "Арефьев Антон Владимирович",
            "Содомовский Никита Максимович", "Молькова Анастасия Дмитриевна", "Маленёва Анастасия Павловна", "Голубков Матвей Германович", "Мартынова Наталия Андреевна",
            "Крашенинников Владислав Александрович", "Советов Михаил Николаевич", "Пярина Екатерина Радионовна", "Усов Алексей Николаевич", "Пчельникова Наталья Дмитриевна",
            "Жигалева Анастасия Алексеевна", "Клищ Татьяна Игоревна", "Демина Александра Вячеславовна", "Новосельцева Анастасия Александровна", "Мохнина Евгения Константиновна",
            "Злобина Елена Валерьевна", "Бобкова Дарья Дмитриевна", "Константинова Анна Романовна", "Тингаева Полина Сергеевна", "Крамков Вячеслав Андреевич", "Суренкова Алла Александровна",
            "Лопатин Михаил Павлович", "Парфенычева Екатерина Александровна", "Калягина Кристина Александровна", "Вьюнова Дарья Владимировна", "Соколова Любовь Сергеевна", "Кусакина Анна Кирилловна",
            "Свиридюк Виктория Владимировна", "Синицына Дарья Алексеевна", "Кутергина Екатерина Вадимовна", "Земляк Виктория Михайловна", "Шакирова Карина Александровна",
            "Лепигина Анастасия Анатольевна", "Савкина Виктория Александровна", "Сорокин Семен Александрович"};
    public static String[] gr = {"TR-1", "TR-2", "TR-3"};
    private static String groupsFile = "groups.txt";
    private static String studentsFile = "students.txt";
    private static String xmlFile = "dekanat.xml";


    public static void main(String[] args) {
        ApplicationDemo application = new ApplicationDemo();

        // Создание исходных файлов
        System.out.println("\n Из имеющихся массивов создаём файлы со списком студентов и списком названий групп");
        application.saveInitialFile(gr, new File(groupsFile));
        application.saveInitialFile(st, new File(studentsFile));

        // загрузить из файлов названия групп и фамилии студентов, создать dekanat с пустыми группами и студентами без групп
        Dekanat dekanat = new Dekanat();
        dekanat.createDekanat();
        application.describeDekanat(dekanat);

        // присвоить студентам id
        int id = 1;
        for (Student st : dekanat.getStudents()) {
            st.setId("" + id);
            id++;
        }
        System.out.println("\n" + "Присвоили студентам id" + "\n");
        application.describeDekanat(dekanat);

        // распределение студентов по группам
        System.out.println("\n Случайное распределение студентов по групам ");
        int numberGroups = dekanat.getGroups().size(); //количество групп
        int numberStudents = dekanat.getStudents().size(); // количество студентов
        for (int i = 0; i < numberStudents; i++)
            dekanat.moveStudentToGroup(dekanat.getStudents().get(i), dekanat.getGroups().get(((int) (Math.random() * numberGroups))));
        application.describeDekanat(dekanat);

        // перевод студента в другую группу
        Student moveStudent = dekanat.getStudents().get(4);
        Group destinationGroup = dekanat.getGroups().get(((int) (Math.random() * numberGroups)));
        System.out.println("\n Студент " + moveStudent.getId()+ " " + moveStudent.getFio() + " переводится в гуппу " + destinationGroup.getTitle());
        dekanat.moveStudentToGroup(moveStudent, destinationGroup);
        application.describeDekanat(dekanat);

        //поиск студента
        System.out.println("\n Поиск студента по id или фамилии \n");
        String search = "3";
        application.searchStudent(dekanat, search);

        System.out.println();
        search = "Сорокин Семен Александрович";
        application.searchStudent(dekanat, search);

        System.out.println();
        search = "Тернова";
        application.searchStudent(dekanat, search);

        System.out.println();
        search = "аврпрварт";
        application.searchStudent(dekanat, search);

        // удалить студента
        System.out.println("\n Удаление студента с id = 1");
        dekanat.deleteStudent("1");
        System.out.println("\n Состав групп после удаления студента \n");
        application.describeDekanat(dekanat);

        System.out.println("\n Пытаемся найти удалённого студента по id \n");
        search = "1";
        application.searchStudent(dekanat, search);


        // добавление нового студента, присвоение id и приписка к группе
        System.out.println("\n Добавление нового студента, присвоение id и приписка к группе\n");
        System.out.println(" Добавим студента Сметанин Петр Егорович\n");

        String idNewStudent = dekanat.searchMaxID(); // поиск id на 1 больше максимального
        Student newStudent = dekanat.createNewStudent("Сметанин Петр Егорович");//создание студента
        newStudent.setId(idNewStudent); // присвоен ID
        dekanat.moveStudentToGroup(newStudent, dekanat.getGroups().get((int) (Math.random() * numberGroups))); // студента поместили в группу, выбранную случайно
        application.describeDekanat(dekanat);

        // выставление студентам оценок
        System.out.println("\n Выставление студентам оценок\n");
        dekanat.generateMarks();
        application.describeDekanat(dekanat);

        System.out.println(dekanat.getGroups().get(1).getNum());

        // студенты со средним баллом меньше 3 отчисляются
        System.out.println("\n Определить и отчислить неуспевающих студентов\n");
        dekanat.expelStudent();
        application.describeDekanat(dekanat);

        // выборы старост групп
        System.out.println("\n Выборы старост групп\n");
        dekanat.startElection();
        application.describeDekanat(dekanat);

        dekanat.marshall(); // сохранение в xml файл

// создание нового деканата и загрузка в него данных из xml файла
        Dekanat dekanatCopy = new Dekanat();
        try {
            dekanatCopy = (Dekanat) dekanat.unmarshal();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
// вывод информации о деканатах
        System.out.println("Деканат\n");
        application.describeDekanat(dekanat);
        System.out.println("\n\nКопия деканата из xml файла\n");
        application.describeDekanat(dekanatCopy);

        //проверка идентичности полей объектов
        System.out.println("\nПроверка идентичности полей объектов\n");
        boolean isTestOK = false;
        if (dekanat.getGroups().size() == dekanatCopy.getGroups().size())
            isTestOK = true;
        System.out.println("Количество групп - " + isTestOK);
        isTestOK = false;

        if (dekanat.getStudents().size() == dekanatCopy.getStudents().size())
            isTestOK = true;
        System.out.println("Количество студентов - " + isTestOK);
        isTestOK = false;

        for (int j = 0; j < dekanat.getGroups().size(); j++) {
            isTestOK = false;
            if(dekanat.getGroups().get(j).getTitle().equals(dekanat.getGroups().get(j).getTitle()))
                isTestOK = true;
            System.out.println("Группа  " + isTestOK);

            if(dekanat.getGroups().get(j).getStudents().equals(dekanatCopy.getGroups().get(j).getStudents()))
                isTestOK = true;
            System.out.println("Студенты  " + isTestOK);

        }
        for (int i = 0; i < dekanat.getStudents().size(); i++) {
            System.out.println("Студент № " + i);
            if (dekanat.getStudents().get(i).getId().equals(dekanatCopy.getStudents().get(i).getId()))
                isTestOK = true;
            System.out.println("ID  " + isTestOK);

            isTestOK = false;
            if (dekanat.getStudents().get(i).getFio().equals(dekanatCopy.getStudents().get(i).getFio()))
                isTestOK = true;
            System.out.println("ФИО  " + isTestOK);

            isTestOK = false;
            if (dekanat.getStudents().get(i).getGroup().equals(dekanatCopy.getStudents().get(i).getGroup()))
                isTestOK = true;
            System.out.println("Группа  " + isTestOK);

            isTestOK = false;
            if (dekanat.getStudents().get(i).getAverageScore() == dekanatCopy.getStudents().get(i).getAverageScore())
                isTestOK = true;
            System.out.println("Средний балл  " + isTestOK);
        }

//        // удаляем Деканат
//        System.out.println("\nУдалить деканат\n");
//        dekanat.setGroups(null);
//        dekanat.setStudents(null);
//        dekanat = null;
//        application.describeDekanat(dekanat);
//        System.out.println("Деканат удалён");
//
//

    }

    // описание состояния деканата
    public void describeDekanat(Dekanat dekanat) {
        try {
            dekanat.describeStudent();
            dekanat.describeGroup();
        } catch (NullPointerException e) {
            System.out.println("Информация о деканате не найдена");
        }
    }

    // форматирует реакцию на запросы поиска студентов
    public Student searchStudent(Dekanat dekanat, String parameter) {
        Student searchSt = dekanat.searchStudent(parameter);
        System.out.println("Поисковый зарос: " + parameter);
        System.out.println("Найден студент:");
        if (searchSt != null) {
            System.out.println(searchSt.getId() + " " + searchSt.getFio());
            return searchSt;
        } else System.out.println("Студент не найден");
        return searchSt;
    }

    // запись в файл исходных данных (ФИО студентов и названия групп) из массивов students и groups
    public void saveInitialFile(String[] content, File file) {
        try {
            FileWriter writer = new FileWriter(file);
            for (String g : content) {
                writer.write(g + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println();
        System.out.println("Создан файл " + file );
    }


}
