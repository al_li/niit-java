import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

import static java.lang.Math.random;

public class ServerOne extends Thread {
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private ArrayList<String> aphorisms;

    public ServerOne(Socket s, ArrayList<String> aphorisms) throws IOException {
        this.aphorisms = aphorisms;
        socket = s;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        start();
    }

    public void run() {
        try {
            while (true) {
                String str = in.readLine();
                if (str.equalsIgnoreCase("end"))
                    break;
                System.out.println("Получено :" + str);
                int rnd = (int) (random() * aphorisms.size());
             //   System.out.println("rnd = " +rnd);
                out.println(aphorisms.get(rnd));
            }
            System.out.println("Соединение закрыто");
        } catch (IOException e) {
            System.err.println("Ошибка чтения / записи");
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                System.err.println("Сокет не закрыт");
            }
        }
    }

}
