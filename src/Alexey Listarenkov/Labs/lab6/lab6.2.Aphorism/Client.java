import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    static final int PORT = 1234;

    public static void main(String[] args) throws IOException {

        System.out.println("Клиент стартовал");
        Socket server = null;

        System.out.println("Соединяемся с сервером localhost");

        server = new Socket("localhost", PORT);
        BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
        PrintWriter out = new PrintWriter(server.getOutputStream(), true);
        BufferedReader inu = new BufferedReader(new InputStreamReader(System.in));
        String fuser;
        final String[] fserver = new String[1];


        JFrame myWindow = new JFrame("Клиент");
        myWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        myWindow.setSize(600, 400);

        myWindow.setLayout(new FlowLayout());

        JButton btn1 = new JButton("Запрос на сервер");
        JButton btn2 = new JButton("Close");

        JPanel labelPanel = new JPanel();
        labelPanel.setBorder(BorderFactory.createTitledBorder("Афоризм"));

        JLabel label = new JLabel();

        label.setPreferredSize(new Dimension(550, 250));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);
        labelPanel.add(label);

        myWindow.add(btn1);
        myWindow.add(btn2);
        myWindow.add(labelPanel, BorderLayout.SOUTH);

        myWindow.setResizable(false);
        myWindow.setVisible(true);

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                out.println("give me aphorism");
                try {
                    fserver[0] = in.readLine();
                } catch (IOException e1) {
                    System.out.println("Соединение с сервером прекращено");
                }
                // System.out.println(fserver[0]);
                label.setText("<html>" + fserver[0] + "</html>");
            }
        });

        Socket finalServer = server;
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                out.println("end");
                try {
                    fserver[0] = "Соединение с сервером прекращено";
                    out.close();
                    in.close();
                    inu.close();
                    finalServer.close();
                    label.setText(fserver[0]);
                } catch (IOException ex) {
                    System.out.println("Соедиенение не закрыто");
                }
                btn1.removeAll();
            }
        });
    }
}