import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Server {
    static final int PORT = 1234;
    private static ArrayList<String> aphorisms = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Сервер стартовал");
        readRSS();
        try {
            while (true) {
                Socket socket = s.accept();
                try {
                    new ServerOne(socket, aphorisms);
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            s.close();
        }
    }

    // соедиенение с сервером RSS рассылки афоризмов
    private static void readRSS() throws IOException {
        URL site = new URL("http://bash.im/rss/");
        String str;
        int i = 0;
        CharSequence sequence = "![CDATA[";
        Scanner scanner = new Scanner(site.openStream(), "WINDOWS-1251");
        while (!scanner.nextLine().equals("</rss>")) {
            str = scanner.nextLine();
            if (str.contains(sequence)) {
                String tmp;
                String aphorism = (tmp = str.substring(0, str.lastIndexOf(']') - 1)).substring(tmp.lastIndexOf('[') + 1);
                aphorisms.add(aphorism);
            }
        }

        scanner.close();
    }
}
