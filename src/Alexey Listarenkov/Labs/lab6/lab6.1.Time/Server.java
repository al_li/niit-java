import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public class Server {
    static final int PORT = 51583;

    public static void main(String[] args) throws IOException {
        System.out.println("Старт сервера");

        // поток для чтения данных
        BufferedReader in = null;
        // поток отправки данных
        PrintWriter out = null;
        // серверный сокет
        ServerSocket server = null;
        // клиентский сокет
        Socket client = null;

        // создание серверного сокета
       try {

//           server = new ServerSocket(0); // автоматическое назначение порта
//           int PORT = server.getLocalPort();
//           System.out.println("Сервер настроился на порт "+ PORT);
           server = new ServerSocket(PORT);
       } catch (IOException e){
           System.out.println("Ошибка связывания с портом " + PORT);
           System.exit(-1);
       }

       try{
           System.out.println("Ожидаю соединения");
           client = server.accept();
           System.out.println("Клиент подключился");
       }catch (IOException e){
           System.out.println("Не могу установить соединение");
           System.exit(-1);
       }
       // поток для связи с клиентом
        in = new BufferedReader(
                new InputStreamReader(client.getInputStream()));
       out = new PrintWriter(client.getOutputStream(), true);
       String input, output;

       // ожидание сообщений от клиента
        System.out.println("Ожидаю запрос");
        while((input = in.readLine())!=null){
            if (input.equalsIgnoreCase("exit"))
                break;
            output = (new Date()).toString();
            out.println("Сервер: " + output);
            System.out.println(input);
        }
        // закрытие соединений
        out.close();
        in.close();
        client.close();
        server.close();
    }
}
