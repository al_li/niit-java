import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    static final int PORT = 51583;

    public static void main(String[] args) throws IOException {

        System.out.println("Клиент стартовал");
        Socket server = null;

        System.out.println("Соединяемся с сервером localhost");

        server = new Socket("localhost", PORT);
        BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
        PrintWriter out = new PrintWriter(server.getOutputStream(), true);
        BufferedReader inu = new BufferedReader(new InputStreamReader(System.in));
        String fuser;
        final String[] fserver = new String[1];


        JFrame myWindow = new JFrame("Клиент");
        myWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        myWindow.setSize(400, 150);

        myWindow.setLayout(new FlowLayout());

        JButton btn1 = new JButton("Запрос на сервер");
        JButton btn2 = new JButton("Close");
        JLabel label = new JLabel();

        myWindow.add(btn1);
        myWindow.add(btn2);
        myWindow.add(label);

        myWindow.setVisible(true);

//        while ((fuser = inu.readLine()) != null) {
//            out.println(fuser);
//            fserver[0] = in.readLine();
//            System.out.println(fserver[0]);
//            label.setText(fserver[0]);
//            if (fuser.equalsIgnoreCase("exit"))
//                break;
//        }

        btn1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                out.println("data and time");
                try {
                    fserver[0] = in.readLine();
                } catch (IOException e1) {
                    System.out.println("Соединение с сервером прекращено");;
                }
               // System.out.println(fserver[0]);
                label.setText(fserver[0]);
            }
        });

        Socket finalServer = server;
        btn2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    out.println("exit");
                    fserver[0] = "Соединение с сервером прекращено";
                    out.close();
                    in.close();
                    inu.close();
                    finalServer.close();
                    label.setText(fserver[0]);
                } catch (IOException ex) {
                    System.out.println("Соедиенение не закрыто");
                }
                btn1.removeAll();
            }
        });
    }
}
