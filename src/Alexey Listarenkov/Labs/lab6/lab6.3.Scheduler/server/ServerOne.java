package server;

import server.scheduler.Person;
import server.scheduler.Tasks;

import java.io.*;
import java.net.Socket;
import java.util.*;

public class ServerOne extends Thread {

    final private static String[] ID = {"1", "2", "3", "4", "5"};
    private static String[] clientState = {"off", "off", "off", "off", "off"}; // 0 - клиент не подлключен, 1 - клиент подключен

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private ArrayList<Person> personList;


    public ServerOne(Socket s, ArrayList<Person> personList) throws IOException {
        socket = s;
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        this.personList = personList;
        start();
    }

    public void run() {
        String id = null;
        try {
            id = in.readLine();
            if (isTrueID(id) && isClientOff(id)) {
                clientState[Integer.parseInt(id) - 1] = "on";
                System.out.println("Подключился клиент id:" + id);
                Person person = personList.get(Integer.parseInt(id) - 1);
                out.println("Здравствуйте " + person.getName());
                while (true) {
                    for (Tasks task : person.getTasks()) {
                        Date taskTime = task.getTaskTime().getTime();
                        Timer timer = new Timer();
                        Date currentTime = new Date(System.currentTimeMillis());
                        if(taskTime.getTime() > currentTime.getTime()) {
                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    out.println(task.getTaskTime().getTime() + " : " + task.getTaskName());
                                    System.out.println(task.getTaskTime().getTime() + " : " + task.getTaskName());
                                }
                            }, taskTime);
                            System.out.println("оповещение на задачу установлено");
                        }
                    }
                    if(in.readLine().equals("end"))break;
                }
                System.out.println("Соединение закрыто");
                clientState[Integer.parseInt(id) - 1] = "off";
                out.println("Соединение закрыто");
                in.close();
                out.close();
            } else {
                System.out.println("Неверный id");
                out.println("Неверный id");
                if (!isClientOff(id))
                    out.println("Запрещено создавать более одного \nподключения с одинаковым id");
                out.println("Клиент уже зарегистрирован в системе");
                clientState[Integer.parseInt(id) - 1] = "off";
                in.close();
                out.close();
            }

        } catch (IOException e) {
            System.err.println("Ошибка чтения / записи");
            clientState[Integer.parseInt(id) - 1] = "off";
        } finally {
            try {
                socket.close();
                System.out.println("Клиент id:" + id + " Отключился. Сокет закрыт");
                clientState[Integer.parseInt(id) - 1] = "off";
            } catch (IOException e) {
                System.err.println("Сокет не закрыт");
            }
        }
    }

    private boolean isClientOff(String id) {
        if (clientState[Integer.parseInt(id) - 1].equals("off")) return true;
        return false;
    }

    private boolean isTrueID(String id) {
        for (String s : ID) {
            if (s.equals(id)) return true;
        }
        return false;
    }

}
