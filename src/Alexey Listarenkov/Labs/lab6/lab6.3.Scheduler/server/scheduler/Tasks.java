package server.scheduler;

import java.util.Calendar;

public class Tasks {
    private String taskName;
    private Calendar taskTime;

    public Tasks(String taskName, Calendar taskTime) {
        this.taskName = taskName;
        this.taskTime = taskTime;
    }

    public String getTaskName() {
        return taskName;
    }

    public Calendar getTaskTime() {
        return taskTime;
    }
}
