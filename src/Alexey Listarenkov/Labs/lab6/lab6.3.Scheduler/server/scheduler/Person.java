package server.scheduler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;

public class Person {
    private String id;
    private String name;
    private ArrayList<Tasks> tasks = new ArrayList<>();


    public Person(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Tasks> getTasks() {
        return tasks;
    }

    public void addTask(String taskName, Calendar taskDate) {
        tasks.add(new Tasks(taskName, taskDate));
        sort();
    }

    // сортировка задач по дате
    private void sort() {
        tasks.sort(new Comparator<Tasks>() {
            @Override
            public int compare(Tasks o1, Tasks o2) {
                return o1.getTaskTime().compareTo(o2.getTaskTime());
            }
        });
    }

    public String toString() {
        String str, tasksList;
        str = "id: " + getId() + " " + getName() +
                "\nЗадачи:\n";
        for (Tasks task : tasks) {
            str =str + task.getTaskTime().getTime() + " " + task.getTaskName() + "\n";
        }

        return str;
    }
}
