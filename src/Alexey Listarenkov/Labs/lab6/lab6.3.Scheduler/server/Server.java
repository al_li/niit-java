package server;

import server.scheduler.Person;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;

public class Server {
    private static final int PORT = 1236;
    private static ArrayList<Person> personList = new ArrayList<>();

    public static void main(String[] args) throws IOException {


        createScheduler();

        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Сервер стартовал");

        try {
            while (true) {
                Socket socket = s.accept();
                try {
                    new ServerOne(socket, personList);
                } catch (IOException e) {
                    socket.close();
                }
            }
        } finally {
            s.close();
        }

    }

    private static void createScheduler() {
        String[] names = {"Иванов И.И", "Петров А.А.", "Сидоров П.В.", "Васечкин Н.К.", "Крошечкина Э.С."};
        for (int i = 1; i < names.length + 1; i++) {
            Person person = new Person("" + i, names[i - 1]);
            createTasks(person);
            personList.add(person);
            System.out.println(person.toString());
        }
    }

    private static void createTasks(Person person) {
        String[] tasks = {"позвонить", "отправить email", "переговоры", "обед", "совещание", "заполнить отчёт", "подготовить прогноз", "получить з/п"};
        for (String s : tasks) {
            person.addTask(s, setNewDate());
        }
    }

    private static Calendar setNewDate() {
        Calendar cal = Calendar.getInstance();

        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int date = cal.get(Calendar.DATE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);
//        System.out.println(year + " " + month + " " + date + " " + hour + " " + min + " " + sec);
        // для события задаётся случайное время в интервале 5 минут от текущего времени
        int timeInterval = 600; // интервал времени в секундах относительно текущего времени, в котором будут распределяться события
        int timeShift = (int) (Math.random() * timeInterval); // сдвиг времени в секундах
        int timeShiftSec = timeShift % 60;
        int timeShiftMin = (timeShift - timeShiftSec) / 60;
        //       System.out.println(timeShift + " " + timeShiftSec + " " + timeShiftMin);
        if ((sec + timeShiftSec) < 60) {
            sec += timeShiftSec;
        } else {
            sec = sec + timeShiftSec - 60;
            min++;
        }
        if ((min + timeShiftMin) < 60) {
            min += timeShiftMin;
        } else {
            min = min + timeShiftMin - 60;
            hour++;
        }
        //       System.out.println(year + " " + month + " " + date + " " + hour + " " + min + " " + sec);
        cal.set(year, month, date, hour, min, sec);
//        System.out.println("***********************************************");
        return cal;
    }
}
