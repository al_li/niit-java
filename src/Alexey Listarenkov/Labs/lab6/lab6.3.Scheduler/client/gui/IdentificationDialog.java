package client.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class IdentificationDialog extends JDialog {
    private String id;

    // диалоговое окно ввода id
    public IdentificationDialog(JFrame owner) {
        super(owner, "Идентификация клиента", true);
        setLayout(new FlowLayout());
        add(new JLabel("Введите id (1...5)"));
        JTextField textField = new JTextField();
        textField.setPreferredSize(new Dimension(100, 20));
        textField.setMinimumSize(new Dimension(100, 20));
        textField.setMaximumSize(new Dimension(100, 20));

        textField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    setVisible(false);
                    id = textField.getText();
                    owner.setTitle("Клиент. ID: " + id);
                    owner.setVisible(true);
                }
            }
        });

        add(textField);

        JButton ok = new JButton("ok");
        ok.doClick();
        ok.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                id = textField.getText();
                    setVisible(false);
                    owner.setTitle("Клиент. ID: " + id);
                    owner.setVisible(true);

            }
        });

        add(ok);
        setSize(350, 80);
        setResizable(false);
    }
    public String getID() {
        return id;
    }
}
