package client.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class MainWindow extends JFrame {
    JLabel label = new JLabel();

    public MainWindow() {
        super();

        //   setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setSize(450, 300);
        setLayout(new FlowLayout());

        JPanel labelPanel = new JPanel();
        labelPanel.setBorder(BorderFactory.createTitledBorder("Напоминание"));

        label.setPreferredSize(new Dimension(350, 200));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setVerticalAlignment(SwingConstants.CENTER);
        labelPanel.add(label);
        add(labelPanel);
        setResizable(false);

        addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("windowClosing");


                System.exit(0);
            }

            @Override
            public void windowClosed(WindowEvent e) {

            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
    }

    public void setLabelText(String text) {
        label.setText(text);
    }

    public String getLabelText() {
        return getTitle();
    }
}

