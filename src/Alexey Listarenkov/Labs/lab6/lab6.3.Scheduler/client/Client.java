package client;

import client.gui.IdentificationDialog;
import client.gui.MainWindow;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;

public class Client {
    static final int PORT = 1236;

    public static void main(String[] args) throws IOException, NullPointerException {
        String id;

        //инициализация окон программы
        MainWindow mainWindow = new MainWindow();
        JDialog dialog = new IdentificationDialog(mainWindow);
        dialog.setVisible(true);
        System.out.println(mainWindow.getTitle());

        System.out.println("Клиент стартовал");
        Socket server = null;

        System.out.println("Соединяемся с сервером localhost");

        id = mainWindow.getLabelText().substring(12);
        System.out.println(id);
        //соединение с сервером
        try {
            server = new Socket("localhost", PORT);
        } catch (ConnectException e) {
            System.out.println("Сервер недоступен");
            mainWindow.setLabelText("Сервер  недоступен");
        }

        //инициализация потоков обмена информации с сервером
        BufferedReader in = new BufferedReader(new InputStreamReader(server.getInputStream()));
        PrintWriter out = new PrintWriter(server.getOutputStream(), true);
        final String[] fserver = new String[1];

        out.println(id); // id передаётся на сервер

        while (true) {
            try {
                fserver[0] = in.readLine();
                if(fserver[0]==null) break;
                System.out.println(fserver[0]);
                mainWindow.setLabelText(fserver[0]);
            } catch (IOException e1) {
                System.out.println("Соединение с сервером прекращено");
                break;
            }
        }

        // out.println("end");
        try {
            System.out.println("Закрываю соединение с сервером");
            out.close();
            in.close();
            server.close();

        } catch (IOException ex) {
            System.out.println("Соедиенение не закрыто");
        }
    }
}