class Circle {
	private double Radius;
	private double Ference;
	private double Area;
	
	void SetRadius(double r) {
		Radius = r;
		Ference = 2*Math.PI*r;
		Area = r*r*Math.PI;		
	}
	void SetFerence(double l) {
		Ference = l;
		Radius = l/(2*Math.PI);
		Area = l*l/(4*Math.PI);	
	}
	void SetArea(double s) {
		Area = s;
		Radius = Math.pow(s/Math.PI,0.5);
		Ference = 2*Math.PI*Math.pow(s/Math.PI,0.5);	
	}	
	double GetRadius() {
		return Radius;			
	}
	double GetFerence() {
		return Ference;			
	}
	double GetArea() {
		return Area;			
	}	
}
public class Pool {
	
	public static void main(String[] args) {
		Circle c1 = new Circle();
		c1.SetRadius(3);		
		Circle c2 = new Circle();
		c2.SetRadius(c1.GetRadius()+1);
		double CostTrack = 1000 * (c2.GetArea()-c1.GetArea());
		double CostFence = 2000 * c2.GetFerence();
		System.out.format("��������� ��������� ����������: %.3f ������", (CostTrack+CostFence));
	}
}