import java.util.Random;

class Group {
	String Title;
	int Num = 0;
	Student[] Students = new Student[30];
	Student Head;
	
	Group (String Title1) {
		Title = Title1;
	}
	
	String getTitle() {return Title;}
	
	void addStudent(Student st) {
		Students[Num] = st;
		Num++;
	}
	boolean findStudent(int id) {
		for (int i=0; i<Num; i++) {
			if (Students[i].getID() == id) {
				return true;
				//System.out.println("true");
				}
		}
		return false;
		//System.out.println("false");
	}
	float averageMarksGroup() {
		float average = 0;
		float sum = 0;
		for (int i=0; i<Num; i++) {
			sum = sum + Students[i].averageMarks();
		}
		average = sum/Num;
		return average;
	}
	void removeStudent(Student st) {
		int n=0;
		for (int i=0; i<Num; i++) {
			if (Students[i]==st) {
				n=i;
				break;
			}
		}
		for (int i=n; i<Num; i++) {
			Students[i] = Students[i+1];
		}
		Num=Num-1;
			
				
	}
	
	void setHead() {
		Random random = new Random();
		Head = Students[random.nextInt(Num)];
	}
}