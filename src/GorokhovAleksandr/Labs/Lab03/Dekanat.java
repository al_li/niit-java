import java.io.*;
import java.util.Random;

class Dekanat {
	Student[] Students = new Student[30];
	
	Group[] Groups = new Group[3];
	
	int Num = 0;
	
	void addStudent() {
		try {
			File f = new File ("students.txt");
			FileReader reader = new FileReader(f);
			BufferedReader breader = new BufferedReader(reader);
			String line;
			String[] lineArr;
			int i = 0;
			while ((line=breader.readLine()) != null) {
				lineArr = line.split(" ");
				
				Students[i] = new Student(Integer.parseInt(lineArr[0]), lineArr[1]);
				
				i++;
				Num++;
			}
			reader.close();
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			
		}
		
		
	}
	
	void addGroup() {
		try {
			File f = new File ("groups.txt");
			FileReader reader = new FileReader(f);
			BufferedReader breader = new BufferedReader(reader);
			String line;
			
			int i = 0;
			while ((line=breader.readLine()) != null) {					
				Groups[i] = new Group(line);				
				//System.out.println(Groups[i].getTitle());
				i++;
			}
			reader.close();
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());
			
		}
		
	}
	
	void addRandomMarks () {
		Random random = new Random();
		for (int i=0; i<Students.length; i++) {			
			Students[i].setMarks(random.nextInt(4) + 2);
			Students[i].setMarks(random.nextInt(4) + 2);
			Students[i].setMarks(random.nextInt(4) + 2);
			Students[i].setMarks(random.nextInt(4) + 2);
			Students[i].setMarks(random.nextInt(4) + 2);
			Students[i].setMarks(random.nextInt(4) + 2);
			//System.out.println(Students[i].averageMarks());
		}
		
	}
	
	
	
	void moveStudent(Student st1, Group gr1) {
		
		st1.getGroup().removeStudent(st1);
		st1.setGroup(gr1);		
		gr1.addStudent(st1);		
	}
	
	void removeStudent(Student st1) {
		
		st1.getGroup().removeStudent(st1);				
		
		int n=0;
		for (int i=0; i<Num; i++) {
			if (Students[i]==st1) {
				n=i;
				break;
			}
		}
		for (int i=n; i<Num-1; i++) {
			Students[i] = Students[i+1];
		}
		Num=Num-1;		
	}
	
	void RefreshFile() {
		try {
			FileWriter writer = new FileWriter("students2.txt");		
			String text = "";
			for (int i=0; i<Num; i++) {
				text = Students[i].getID() + " " + Students[i].getFio() + " " + Students[i].getGroup().getTitle() + "\n";
				writer.write(text);
			}			
			
			writer.flush();			
					
		}
		catch(IOException ex) {
			System.out.println(ex.getMessage());			
		}
		
	}
	
	void PrintStudent() {
		
		for (int j=0; j<Groups.length; j++) {
			System.out.println("������ - " + Groups[j].getTitle());
			System.out.println("���������� ��������� � ������ - " + Groups[j].Num);
			System.out.println("�������� - " + Groups[j].Head.getFio());
			System.out.println("������� ������ �� ������ - " + Groups[j].averageMarksGroup());
			
			for (int i=0; i<Groups[j].Num; i++) {
				System.out.println(Groups[j].Students[i].getFio() +  " ������� ������ - " + Groups[j].Students[i].averageMarks() );
			}
			System.out.println("\n");
			//System.out.println("\n" + Groups[i].Head.getFio());
		}
		
	}
	
	void setHeadGroup() {
		for (int i=0; i<Groups.length; i++) {
			Groups[i].setHead();
			
		}
		
	}
	
}