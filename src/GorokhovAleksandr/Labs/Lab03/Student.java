class Student {
	int ID;
	String Fio;
	Group group;
	int[] Marks = new int[10];
	int Num = 0;		
	
	Student(int ID1, String Fio1) {
		ID = ID1; Fio = Fio1;
	}
	int getID() {return ID;}
	String getFio() {return Fio;}
	
	void setGroup (Group gr) {
		this.group = gr;
	}
	Group getGroup() {return group;}
	
	void setMarks(int Mark) {		
		Marks[Num] = Mark;
		//System.out.println(Marks[i]);
		Num++;		
	}
	float averageMarks() {
		float average = 0;
		float sum = 0;
		if (Num>0) {
			for (int i=0; i<Num; i++) {
				sum = sum + Marks[i];
			}
			average = sum/Num;
		}
		return average;
			
	}
}