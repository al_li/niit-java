import java.io.*;
import java.util.Random;


public class Staff {
	//double payment=0;
	static int[] ProjPay = {500000, 400000, 800000};
	static int ProjNumPerson = 4;
	//int Hour = 176;
	
	static int N = 100;
	static int NumEmployee = 0;
	static Employee[] emp = new Employee[N];
	//static String[] lineArr;

	
	
	public static void AddEmployee() {
		try {
            File file = new File("Employee.txt");			
            FileReader reader = new FileReader(file);
            BufferedReader breader = new BufferedReader(reader);
            String line;
            int i = 0;
			int id;
			int base;
			int percentWork;
			int costProject;
			Random random = new Random();
            while ((line = breader.readLine()) != null) {
                String[] lineArr = line.split("\t");
                id = Integer.parseInt(lineArr[0]);                          
                String name = lineArr[1];                                     
                String profession = lineArr[2];                                
                base = Integer.parseInt(lineArr[3]);                       
                String project = lineArr[4];				
				switch (lineArr[4]) {
					case "project1":
						costProject = ProjPay[0];						
						break;	
					case "project2":
						costProject = ProjPay[1];						
						break;	
					case "project3":
						costProject = ProjPay[2];						
						break;
					default:
						costProject = 0;
						break;
				}
				switch (lineArr[2]) {						
					case "programmer":
						percentWork = random.nextInt(7);
						emp[i] = new Programmer(id, name, base, project, costProject, percentWork);							
						break;						
					case "teamleader":
						percentWork = random.nextInt(7);
						emp[i] = new TeamLeader(id, name, base, project, costProject, percentWork);						
						break;
					case "tester":
						percentWork = random.nextInt(7);
						emp[i] = new Tester(id, name, base, project, costProject, percentWork);						
						break;
					case "projectmanager":
						percentWork = random.nextInt(20)+15;
						emp[i] = new ProjectManager(id, name, project, costProject, percentWork,  ProjNumPerson);						
						break;
					case "seniormanager":
						percentWork = random.nextInt(5)+15;
						costProject = ProjPay[0]+ProjPay[1]+ProjPay[2];
						emp[i] = new SeniorManager(id, name, project, costProject, percentWork,  ProjNumPerson*3);						
						break;
					case "cleaner":
						emp[i] = new Cleaner(id, name, base);						
						break;
					case "driver":
						emp[i] = new Driver(id, name, base);
						break;
				}
				i++;
				NumEmployee++;
				
			}
			reader.close();
		}
		catch(Exception ex) {
			System.out.println(ex.getMessage());			
		}
	}			
}