import java.util.Random;

public abstract class Engineer extends Employee implements WorkTime, Project {
	int base;
	String project;
	int costProject;
	int percentWork;
	
	public Engineer(int id, String name, int base, String project, int costProject, int percentWork) {
		super(id, name);		
		this.base = base;
		this.project = project;
		this.costProject = costProject;
		this.percentWork = percentWork;
	}	
	
	
	@Override
	public double CalcWorkTime(int Hour, int base) {
		double worktime = Hour * base;
		return worktime;
	}
	@Override
	public double CalcProject(int costProject, int percentWork) {		
		double project = costProject*percentWork/100;
		return project;
	}
	
	public void Calc(){
		setPayment(CalcWorkTime(worktime,base) + CalcProject(costProject, percentWork));
	}

}

class Programmer extends Engineer {
	public Programmer(int id, String name, int base, String project, int costProject, int percentWork) {
		super(id, name, base, project, costProject, percentWork);
	}	
	
	
}

final class Tester extends Engineer {
	public Tester(int id, String name, int base, String project, int costProject, int percentWork) {
		super(id, name, base, project, costProject, percentWork);
	}	
	
}

final class TeamLeader extends Programmer implements Heading{
	public TeamLeader(int id, String name, int base, String project, int costProject, int percentWork) {
		super(id, name, base, project, costProject, percentWork);
	}	
	public double CalcHeader(int numPeople) {		
		double header = numPeople*1000;
		return header;
	}
	@Override
	public void Calc(){
		setPayment(CalcWorkTime(worktime,base) + CalcProject(costProject, percentWork) + CalcHeader(5));
	}

}