import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class NetDateAndRate extends Application {

    private Label lbDate, lbTime, lbRate;
    private Button bDate, bRate;
    private TextField tfDate, tfTime, tfRate;
    private RadioButton rbRateFromUSD, rbRateFromEUR, rbRateFromRUB, rbRateToRUB, rbRateToEUR, rbRateToUSD;
    private Separator separator1 = new Separator();

    private URL url = null;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private String strDate = null;
    private String strTime = null;

    private String strRate = null;
    private String rub = "RUB", usd = "USD", eur = "EUR";
    private String rateFrom = usd;
    private String rateTo = rub;
    private String addrDate = "http://shtanyuk.tk/";
    private String addrRate = "http://www.webservicex.net/CurrencyConvertor.asmx/ConversionRate?FromCurrency="
            + rateFrom + "&ToCurrency=" + rateTo;

    Font font = Font.font("Arial", FontWeight.BOLD, 25);

    @Override
    public void start(Stage primaryStage) throws IOException{
        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);
        primaryStage.setTitle("NetDateAndRate");
        primaryStage.setScene(new Scene(root, 300, 520));
        primaryStage.setResizable(false);
        root.setPadding(new Insets(20));
        root.setHgap(25);
        root.setVgap(15);

        lbDate = new Label("Текущая дата");
        lbDate.setFocusTraversable(true);
        GridPane.setColumnSpan(lbDate,2);
        GridPane.setHalignment(lbDate, HPos.CENTER);

        lbTime = new Label("Текущее время");
        GridPane.setColumnSpan(lbTime,2);
        GridPane.setHalignment(lbTime, HPos.CENTER);

        bDate = new Button("Узнать текущие дату и время");
        bDate.setCenterShape(true);
        GridPane.setColumnSpan(bDate,2);
        GridPane.setHalignment(bDate, HPos.CENTER);

        tfDate = new TextField();
        tfDate.setPromptText("Текущая дата");
        tfDate.setAlignment(Pos.CENTER);
        tfDate.setPrefSize(230, 25);
        tfDate.setFont(font);
        GridPane.setHalignment(tfDate, HPos.CENTER);
        GridPane.setColumnSpan(tfDate,2);

        tfTime = new TextField();
        tfTime.setPromptText("Текущее время");
        tfTime.setAlignment(Pos.CENTER);
        tfTime.setPrefSize(230, 25);
        tfTime.setFont(font);
        GridPane.setHalignment(tfTime, HPos.CENTER);
        GridPane.setColumnSpan(tfTime,2);

        lbRate = new Label("Выбрать валюты");
        GridPane.setColumnSpan(lbRate,2);
        GridPane.setHalignment(lbRate, HPos.CENTER);

        rbRateFromUSD = new RadioButton(usd);
        rbRateFromUSD.fire();
        rbRateFromUSD.setPrefWidth(100);
        rbRateFromEUR = new RadioButton(eur);
        rbRateFromRUB = new RadioButton(rub);

        rbRateToRUB = new RadioButton(rub);
        rbRateToRUB.fire();
        rbRateToEUR = new RadioButton(eur);
        rbRateToUSD = new RadioButton(usd);

        ToggleGroup tgRateFrom = new ToggleGroup();
        ToggleGroup tgRateTo = new ToggleGroup();

        rbRateFromUSD.setToggleGroup(tgRateFrom);
        rbRateFromEUR.setToggleGroup(tgRateFrom);
        rbRateFromRUB.setToggleGroup(tgRateFrom);

        rbRateToRUB.setToggleGroup(tgRateTo);
        rbRateToEUR.setToggleGroup(tgRateTo);
        rbRateToUSD.setToggleGroup(tgRateTo);

        tfRate = new TextField();
        tfRate.setPromptText("Текущий курс");
        tfRate.setAlignment(Pos.CENTER);
        tfRate.setPrefSize(230, 25);
        tfRate.setFont(font);
        GridPane.setHalignment(tfRate, HPos.CENTER);
        GridPane.setColumnSpan(tfRate,2);

        bRate = new Button("Узнать текущий курс");
        GridPane.setHalignment(bRate, HPos.CENTER);
        GridPane.setColumnSpan(bRate,2);

        bDate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getDateAndTime();
                tfDate.setText(strDate);
                tfTime.setText(strTime);
            }
        });

        bRate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                getRate();
                tfRate.setText(strRate);
            }
        });

        rbRateFromUSD.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rateFrom = usd;
            }
        });

        rbRateFromEUR.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rateFrom = eur;
            }
        });

        rbRateFromRUB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rateFrom = rub;
            }
        });

        rbRateToRUB.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rateTo = rub;
            }
        });

        rbRateToEUR.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rateTo = eur;
            }
        });

        rbRateToUSD.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                rateTo = usd;
            }
        });

        GridPane.setColumnSpan(separator1,2);

        root.add(lbDate, 0, 0);
        root.add(tfDate, 0, 1);
        root.add(lbTime, 0, 2);
        root.add(tfTime, 0, 3);
        root.add(bDate, 0, 4);
        root.add(separator1, 0, 5);
        root.add(lbRate, 0, 6);
        root.add(rbRateFromUSD, 0, 7);
        root.add(rbRateFromEUR, 0, 8);
        root.add(rbRateFromRUB, 0, 9);
        root.add(rbRateToRUB, 1, 7);
        root.add(rbRateToEUR, 1, 8);
        root.add(rbRateToUSD, 1, 9);
        root.add(tfRate, 0, 10);
        root.add(bRate, 0, 11);

        primaryStage.show();

    }

    //получение даты и времени
    public void getDateAndTime() {
        long d = 0;
        try{
            url = new URL(addrDate);
        } catch (MalformedURLException e){System.out.println("Неверный URL");}
        try{
            URLConnection urlCon = url.openConnection();
            d = urlCon.getDate();
        } catch (IOException e){System.out.println("Ошибка ввода-вывода");}

        if (d!=0){
            Date date = new Date(d);
            strDate = dateFormat.format(date);
            strTime = timeFormat.format(date);
        }
        else {
            strDate = "Нет данных";
            strTime = "Нет данных";
        }
    }

    //получение курса
    public void getRate() {
        String answer = "Нет данных";
        try{
            url = new URL(addrRate);
        } catch (MalformedURLException e){System.out.println("Неверный URL");}
        try{
            Scanner sc = new Scanner(url.openStream());
            // <?xml version="1.0" encoding="utf-8"?>
            sc.nextLine();
            // <double xmlns="http://www.webserviceX.NET/">0.724</double>
            answer = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");
            sc.close();
        } catch (IOException e){System.out.println("Ошибка ввода-вывода");}

        if (rateFrom.equals(rateTo)) strRate = "1";
        else strRate = answer;
    }

    //запуск приложения
    public static void main(String[] args) {
        launch(args);
    }
}
