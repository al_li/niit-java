import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class TaskClient {
    public static void main(String[] args) throws IOException{
        Socket server = null;
        String hostName = null;
        int port = 1234;
        if (args.length == 0) hostName = "localhost";
        else hostName = args[0];
        int clientID = 1;

        System.out.println("Клиент стартовал");
        System.out.println("Соединяемся с сервером " + hostName);
        server = new Socket(hostName, port);

        BufferedReader inStream  = new BufferedReader(new InputStreamReader(server.getInputStream()));
        PrintWriter outStream = new PrintWriter(server.getOutputStream(),true);
        BufferedReader userStream = new BufferedReader(new InputStreamReader(System.in));

        String strServer = null;

        //передаем серверу ID клиента
        outStream.println(clientID);

        //получаем сообщения от сервера
        while ((strServer = inStream.readLine()) != null) {
            System.out.println(strServer);
        }
        inStream.close();
        outStream.close();
        userStream.close();
        server.close();
        System.out.println("Клиент отключился");
    }
}

