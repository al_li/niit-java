
class Circle{
	private static double pi=3.14159265;
	private double radius=0.0, ference=0.0, area=0.0;	//инициируем переменные радиус, длина окружности и площадь
		
	double getFerence(double r)	//метод получения длины окружности с помощью радиуса
	{
		this.ference=2*pi*r;
		return this.ference;
	}
	double getArea(double r)	//метод получения площади с помощью радиуса
	{
		this.area=pi*r*r;
		return this.area;
	}
	double getRadiusFromFerence(double f)	//метод получения радиуса с помощью длины окружности
	{
		this.radius=f/(2*pi);
		return this.radius;
	}
	double getRadiusFromArea(double a)	//метод получения радиуса с помощью площади
	{
		this.radius=Math.sqrt(a/pi);
		return this.radius;
	}
}
public class Swimpool{
	public static void main(String[] args){
		//радиус бассейна 3 м, ширина бетонной дорожки 1 метр
		//1 м ограды 2000р, 1 кв м дорожки 1000р 
		
		Circle swimpool = new Circle();	//создаем объект бассейн
		double radiusSwimpool = 3;	//радиус бассейна в метрах
		double wall = swimpool.getFerence(radiusSwimpool);	//получаем длину ограды
		double path = swimpool.getArea(radiusSwimpool+1)-swimpool.getArea(radiusSwimpool);	//получаем площадь дорожки
		double cost = wall*2000+path*1000;	//цена работ
		System.out.format("cost in rub = %.2f%n", cost);	//выводим стоимость работ в рублях с точностью 2 знака после ,
	}
}	