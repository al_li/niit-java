package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class ProgrammerTest extends TestCase {
    public void testCalcPaymentForWorkTime() throws Exception {
        Programmer programmer = new Programmer(1012,"Сорокин С.С.","Programmer",450,1000000,10000,1000);
        programmer.setWorkTime(8);
        assertEquals(3600.0,programmer.calcPaymentForWorkTime());
    }

    public void testCalcPaymentForProject() throws Exception {
        Programmer programmer = new Programmer(1012,"Сорокин С.С.","Programmer",450,1000000,10000,1000);
        assertEquals(35000.0,programmer.calcPaymentForProject());
    }

    public void testCalcPayment() throws Exception {
        Programmer programmer = new Programmer(1012,"Сорокин С.С.","Programmer",450,1000000,10000,1000);
        programmer.setWorkTime(8);
        assertEquals(38600.0,programmer.calcPayment());
    }

}