package kostya;

import junit.framework.TestCase;

/**
 * Created by kostya on 20.04.2017.
 */
public class DriverTest extends TestCase {
    public void testCalcPayment() throws Exception {
        Driver driver = new Driver(1032,"Лосев Л.Л.","Driver",150);
        driver.setWorkTime(8);
        assertEquals(1200.0,driver.calcPayment());
    }

}