package kostya;

// инженер. Имеет ставку и оплату за час + бонусы от выполняемого проекта.
public abstract class Engineer extends Employee implements WorkTime, Project{
    private int base = 0;                           //ставка за час
    private int projectCost = 0;                    //стоимость проекта
    private int numLinesOfCodeInProject = 0;        //количество строк кода в проекте

    public Engineer(int id, String name, String position, int base, int projectCost, int numLinesOfCodeInProject){
        super(id, name, position);
        this.base = base;
        this.projectCost = projectCost;
        this.numLinesOfCodeInProject = numLinesOfCodeInProject;
    }

    //получение ставки
    public int getBase(){
        return base;
    }

    //получение стоимости проекта
    public int getProjectCost(){
        return projectCost;
    }

    //получение количества строк в проекте
    public int getNumLinesOfCodeInProject(){
        return numLinesOfCodeInProject;
    }

    //расчет оплаты исходя из отработанного времени (часы умножаются на ставку)
    @Override
    public double calcPaymentForWorkTime() {
        return getWorkTime() * getBase();
    }

    //расчет зароботной платы
    @Override
    public double calcPayment() {
        setPayment(calcPaymentForWorkTime() + calcPaymentForProject());
        return getPayment();
    }
}
