package kostya;

//расчет оплаты исходя из отработанного времени (часы умножаются на ставку)
public interface WorkTime{
    double calcPaymentForWorkTime();
}
