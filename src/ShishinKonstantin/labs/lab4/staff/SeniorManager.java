package kostya;

//руководитель направления. Должен иметь процент со всех проектов.
public class SeniorManager extends ProjectManager{
    private int allProjectsCost = 0;         //стоимость всех проектов

    public SeniorManager(int id, String name, String position, int projectCost, int allProjectsCost, int numManagersInProject, int numSubordinates){
        super(id, name, position, projectCost, numManagersInProject, numSubordinates);
        this.allProjectsCost = allProjectsCost;
    }

    //расчет оплаты исходя из участия в проекте (руководитель направления получает 30% от стоимости всех проектов)
    @Override
    public double calcPaymentForProject() {
        return allProjectsCost * 0.3;
    }

    //расчет оплаты исходя из руководства (количество подчиненных умножается на коэффициент "руководства")
    @Override
    public double calcPaymentForHeading() {
        return getNumSubordinates() * 15000;
    }
}
