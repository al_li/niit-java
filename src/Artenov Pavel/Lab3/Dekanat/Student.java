package Dekanat2;
/*
Разработать класс Student для хранения информации о студенте.
Перечень полей:
ID-идентификационный номер
Fio-Фамилия и инициалы
Group-ссылка на группу(объект Group)
Marks-массив оценок
Num-колличество оценок
Обеспечить класс следующими методами:
-создание студента с указанием ID и FIO
-зачисление в группу
-добавление оценки
-вычисление средней оценки
 */

public class Student {
    private int num = 5;
    private String name;
    private int id;
    private int[] marks = new int[num];
    private Group groupLinks;


    //Создание студента с указанием ID и FIO
    Student(String name,int id){
        this.name = name;
        this.id = id;
    }
    //Добавление оценки
    public void setMark(int mark,int subject){
        marks[subject]=mark;
    }
    //Зачисление в группу
    public void setGroup(Group group){
        groupLinks = group;
    }
    //Вычисление средней оценки студента
    public double getAvgMark(){
        double sum = 0;
        for(int i=0;i<num;i++)
            sum = sum + marks[i];
        return sum / 5.0;
    }
    public Group getGroup(){
        return groupLinks;
    }
    public String getName(){
        return name;
    }
    public int getId(){
        return id;
    }
}
