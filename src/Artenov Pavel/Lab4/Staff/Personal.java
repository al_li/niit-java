package Staff;

public class Personal extends Employee implements WorkTime{
    int rate;          //ставка за час
    public Personal (int id,String name,int worktime,int rate,String position){
        super(id,name,worktime,position);
        this.rate=rate;
    }
    public int calcPaymentWorktime(){               //расчет оплаты исходя из отработанного времени
        return worktime * rate;
    }
    public int paymentCalc(){                       //расчет заработной платы для Driver,Cleaner
        return worktime * rate;
    }
}

class Cleaner extends Personal {
    static final int rate = 20;                                              //у уборщиц одинаковая ставка
    public Cleaner (int id,String name,int worktime,String position){
        super(id,name,worktime,rate,position);
    }
}
class Driver extends Personal {
    static final int rate = 30;                                              //у водителей одинаковая ставка
    public Driver(int id, String name, int worktime,String position) {
        super(id, name, worktime, rate,position);
    }
}



