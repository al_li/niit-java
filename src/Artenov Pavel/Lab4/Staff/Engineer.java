package Staff;

public class Engineer extends Employee implements WorkTime,Project {
    protected int rate;                                   //ставка за час
    protected int budgetProject;
    protected double coefficientOfParticipation;
    public Engineer(int id,String name,int worktime,int rate,int budgetProject,double coefficientOfParticipation,String position){
        super(id,name,worktime,position);
        this.rate=rate;
        this.budgetProject=budgetProject;
        this.coefficientOfParticipation=coefficientOfParticipation;
    }
    public int calcPaymentWorktime(){                           //расчет зп за отработанное время
        return worktime * rate;
    }
    public double calcPaymentProject(){                         //бонусы от участия в проекте
       return   coefficientOfParticipation * budgetProject;
    }
    public int paymentCalc(){                                  //расчет зп (отработанное время + бонусы)
       return  (int)calcPaymentProject() + calcPaymentWorktime();
    }
}
class Programmer extends Engineer{
    public Programmer(int id,String name,int worktime,int rate,int budgetProject,double coefficientOfParticipation,String position){
        super(id,name,worktime,rate,budgetProject,coefficientOfParticipation,position);
    }
}
class Tester extends Engineer {
    public Tester (int id,String name, int worktime,int rate,int budgetProject,double coefficientOfParticipation,String position){
        super (id,name,worktime,rate,budgetProject,coefficientOfParticipation,position);
    }
}
class TeamLeader extends Programmer implements Heading{
    protected int countSubordinate;
    public TeamLeader(int id,String name,int worktime,int rate,int budgetProject,double coefficientOfParticipation,
                      int countSubordinate,String position){
        super(id,name,worktime,rate,budgetProject,coefficientOfParticipation,position);
        this.countSubordinate=countSubordinate;
    }
    public int calcPaymentHeading() {
        return countSubordinate * ratePerSubordinate;
    }
    //расчет зп отработанное время + бонусы от участия в проекте + зп от кол-ва подчиненных
    public int paymentCalc(){
        return calcPaymentHeading() + ((int)calcPaymentProject()) + calcPaymentWorktime();
    }

}
