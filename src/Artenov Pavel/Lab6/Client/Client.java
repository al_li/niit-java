package Client;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.*;

public class Client {
    private String input;
    private String [] text;
    private Socket socClient;
    private BufferedReader in;
    public static void main(String[] args) {
       Client client = new Client();
       client.go();

    }
    public void go(){
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));

        JPanel panelTimer = new JPanel();
        JPanel panelAphorism = new JPanel();
        JPanel panelEvent = new JPanel();
        JPanel panelWidget = new JPanel();
        JLabel label = new JLabel();

        JTextArea textTime = new JTextArea();
        JTextArea textAphorism = new JTextArea("",2,40);
        textAphorism.setWrapStyleWord(true);
        textAphorism.setLineWrap(true);


        textAphorism.setFont(new Font ("Aphorism Font",Font.BOLD,20));
        textTime.setFont(new Font("New font",Font.BOLD,70));

        panelTimer.add(textTime);
        panelAphorism.add(textAphorism);
        mainPanel.add(panelTimer);
        mainPanel.add(panelAphorism);
        mainPanel.add(panelEvent);
        mainPanel.add(panelWidget);
        frame.add(BorderLayout.CENTER,mainPanel);


        frame.setSize(1200,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);





        try{
            socClient = new Socket("127.0.0.1",9999);
            in=new BufferedReader(new InputStreamReader(socClient.getInputStream()));
            //PrintWriter out = new PrintWriter(socClient.getOutputStream());


            while(true) {
                text = in.readLine().split("\\*");

                if(text[0].equals("Timer")){
                    textTime.setText(text[1]);
                }
                else if(text[0].equals("Aphorism")){
                    input = text[1].substring(0,text[1].length()-5);
                    textAphorism.setText(input);

                }
                else if(text[0].equals("close"))
                    in.close();

           }


        }
        catch (IOException ex){
            ex.printStackTrace();
        }
        finally {
            try{
                in.close();
            }
            catch (IOException ioex){
                System.out.println("Could not close socket");
            }
        }


    }

}










































        /*System.out.println("Клиент стартовал");
        Socket server = null;

        if (args.length==0) {
            System.out.println("Использование: java Client IP");
            System.exit(-1);
        }

        System.out.println("Соединяемся с сервером "+args[0]);

        server = new Socket(args[0],1234);
        BufferedReader in  = new BufferedReader(new  InputStreamReader(server.getInputStream()));
        PrintWriter out = new PrintWriter(server.getOutputStream(),true);
        BufferedReader inu = new BufferedReader(new InputStreamReader(System.in));

        String fuser,fserver;

        while ((fuser = inu.readLine())!=null) {
            out.println(fuser);
            fserver = in.readLine();
            System.out.println(fserver);
            if (fuser.equalsIgnoreCase("close"))
                break;
            if (fuser.equalsIgnoreCase("exit"))
                break;
        }

        out.close();
        in.close();
        inu.close();
        server.close();
    }
}*/