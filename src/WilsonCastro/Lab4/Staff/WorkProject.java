
public class WorkProject {

    String name;
    int price;
    int percentForManager;
    private Employee employees[];

    public WorkProject(String name, int price, int percentForManager, Employee[] employess) {
        this.name = name;
        this.price = price;
        this.percentForManager = percentForManager;
        this.employees = new Employee[50];
    }

    public void addEmployee(Employee employee) {
        for (int i = 0; i < employees.length; i++) {
            if (employees[i] == null) {
                if (employee instanceof Engineer) {
                    ((Engineer) employee).setWorkProject(this);
                } else if (employee instanceof Manager) {
                    ((Manager) employee).setWorkProject(this);
                }
                employees[i] = employee;
                return;
            }
        }

    }

    int getNumOfEmployees() {
        int i = 0;
        for (Employee e : employees) {
            if (e != null) {
                i++;
            }
        }
        return i;
    }

    public void printSalary() {
        System.out.println("Proyect:" + this.name + "\n-----\n");
        System.out.printf("%5s %15s %20s\n", "ID", "FIO", "Position", "Salary");
        System.out.println("***************************");
        int total = 0;
        for (Employee e : employees) {
            if (e != null) {
                System.out.printf("%5s %15s %20s\n", e.id, e.name, e.getClass().getSimpleName());
                System.out.println("----------------------------------------------------------------------------------");
                total += e.calcPayment();
            }
        }
        System.out.println("Total: " + total);
    }
}
