/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

public abstract class Employee {

    protected int id = 0;
    protected String name = "";
    // private String position = "";
    protected int workTime = 0;
    protected int payment = 0;

    public abstract int calcPayment();

    public Employee(int id, String name, int workTime, int payment) {
        this.id = id;
        this.name = name;
        this.workTime = workTime;
        this.payment = payment;
        // this.position=position;
    }

    interface workTime {

        int calcPaymentForWorkTime();
    }

    interface project {

        int calcPaymentForProject();
    }

    interface Heading {

        int CalcPaymentForHeading();
    }
}

abstract class Engineer extends Employee implements project, workTime {

    WorkProject workProj;
    private int percents;

    Engineer(int id, String name, int workTime, int payment, int percents) {
        super(id, name, workTime, payment);
        this.percents = percents;
    }

    public void setWorkProject(workProject workProj) {
        this.workProj = workProj;
    }

    public int calcPaymentForWorkTime() {
        return workTime * payment;
    }

    public int calcPaymentForProject() {
        return workProj.price * percents / 100;
    }

    @Override
    public int calcPayment() {
        return calcPaymentForWorkTime() + calcPaymentForProject();
    }
}

abstract class Manager extends Employee implements Project {

    WorkProject workProj;

    Manager(int id, String name) {
        super(id, name, 0, 0);
    }

    public void setWorkProject(WorkProject workProj) {
        this.workProj = workProj;
    }
}

abstract class Personal extends Employee implements workTime {

    public Personal(int id, String name, int workTime, int payment) {
        super(id, name, workTime, payment);
    }

    public int calcPaymentForWorkTime() {
        return workTime * payment;
    }

   @Override
    public int calcPayment() {
        return calcPaymentForWorkTime();
    }
}
