/*
 Задача 4. Свертка числовых диапазонов.
Написать программу, осуществляющую свертку числовых диапазонов 
(обратная задача к 3).
 */

public class Cverka {

    public static void main(String[] args) {
        int i;
        int start = 0;
        boolean inrange = false;
        int arr[] = new int[getNumOfCommasInArr(args[0].toCharArray()) + 1];
        fillIntArrFromCharArr(arr, args[0].toCharArray());

        for (i = 0; i < arr.length - 1; i++) {
            if (arr[i] + 1 == arr[i + 1]) {
                if (!inrange) {
                    start = i;
                }
                inrange = true;
            } else {
                if (inrange) {
                    if (arr[i] - arr[start] > 1) {
                        System.out.print(arr[start] + "-" + arr[i] + ",");
                    } else {
                        System.out.print(arr[start] + "," + arr[i] + ",");
                    }
                } else {
                    System.out.print(arr[i] + ",");
                }
                inrange = false;
            }
        }
        if (inrange) {
            if (arr[i] - arr[start] > 1) {
                System.out.print(arr[start] + "-" + arr[i]);
            } else {
                System.out.print(arr[start] + "," + arr[i]);
            }
        } else {
            System.out.print(arr[i]);
        }
        System.out.println();
    }

    static int getNumOfCommasInArr(char arr[]) {
        int i, commas = 0;
        for (i = 0; i < arr.length; i++) {
            if (arr[i] == ',') {
                commas++;
            }
        }
        return commas;
    }

    static void fillIntArrFromCharArr(int intarr[], char arr[]) {
        for (int i = 0, num = 0, j = 0; i < arr.length; i++) {
            while (i < arr.length && Character.isDigit(arr[i])) {
                num = arr[i++] - '0' + num * 10;
            }
            intarr[j++] = num;
            num = 0;
        }
    }
}
