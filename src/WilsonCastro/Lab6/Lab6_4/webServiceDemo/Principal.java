/*
Задача 4.
Разработать сетевое приложение, которое предоставляет пользователю возможность 
узнать курс валюты относительно другой валюты.
*/
/**
 * Ejem. del clase Java NIIT. WebService para conversion de moneda
 * http://webservicex.net/new/Home/Index
 * ejemplo tambien se puede ver aqui: https://stackoverflow.com/questions/
 * 19554211/using-web-services-from-http-www-webservicex-net-with-java
 */
//import static com.sun.activation.registries.LogSupport.log;
import static java.rmi.server.LogStream.log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Scanner;

public class Principal {

    private static String from;
    private static String to ;

    public static void main(String[] args) throws IOException {
       
        BufferedReader fromData
                = new BufferedReader(new InputStreamReader(System.in));
         System.out.print("Type valute from convert for example USD, EUR, COP: ");
         from = fromData.readLine();
        BufferedReader toData
                = new BufferedReader(new InputStreamReader(System.in));
       System.out.print("Type valute to convert for example USD, EUR, COP: ");

       to = toData.readLine();
  
    URL url = new URL("http://www.webservicex.net/currencyConvertor.asmx"
            + "/ConversionRate?FromCurrency=" + from + "&ToCurrency=" + to);
    Scanner sc = new Scanner(url.openStream());

    sc.nextLine ();
    String str = sc.nextLine().replaceAll("^.*>(.*)<.*$", "$1");
    sc.close ();
    Double rate = Double.parseDouble(str);
    log("Rate: " + rate);
    System.out.println (rate);
}
}
