
public class avtomat {
    
    enum STATES {
        OFF,
        WAIT,
        ACCEPT,
        CHECK,
        COOK
    }

    String menu[] = {" Горячий шоколат, ", " Двоиной шоколат, ",
            " Черный чай, ", " Зеленйы чай, ", " Горячее молоко, ",
            " Апельсиновый сок, ", " Яблочный сок, ", " Холодный квас, "};
    int prices[] = {30, 35, 20, 20, 40, 30, 30, 25};
    STATES state;
    private int cash;
    int nDrink;
    String Message = null;

    avtomat() {
        cash=0;
        state = STATES.OFF;
    }

    public STATES on() {
        if (state == STATES.OFF) {
            state = STATES.WAIT;
            printState();
            printMenu();
        }
        return state;
    }

    public STATES off() {
        if (state == STATES.WAIT) {
            state = STATES.OFF;
            printState();
        }
        return state;
    }

    public int coin(int cash) {
        this.cash = cash;
        if (state == STATES.WAIT) {
            state = STATES.ACCEPT;
            printState();
            //  this.cash+=cash;
            System.out.println("You put " + this.cash + "rub");
        }
        return this.cash;
    }

    public String[] printMenu() {
        String strMenu[] = new String[menu.length];
        System.out.println("Menu:");
        for (int i = 0; i < menu.length; i++) {
            strMenu[i] = (i + 1) + "-" + menu[i] + prices[i] + "rub.";
            System.out.println(strMenu[i]);
        }
        return strMenu;
    }

    public String printState() {
        String information = "STATE: " + state;
        System.out.println(information);
        return information;
    }

    public void cancel() {  // este lo tome de otro lado
        if (state != STATES.ACCEPT) {
            state = STATES.WAIT;
        }
    }

    public String choise(int nDrink) {
        if (state == STATES.ACCEPT) {
            state = STATES.CHECK;
            printState();
            this.nDrink = nDrink - 1;
            System.out.println("Your order:" + menu[this.nDrink]);
            check();
        }
        return menu[this.nDrink];
    }

    public String check() {
        if (state == STATES.CHECK) {
            state = STATES.CHECK;
            printState();
            if (cash < prices[nDrink]) {
                Message = "Not enought money, insert the necesary amount";
                System.out.println(Message);
                // state = STATES.ACCEPT;
            } else if (cash > prices[nDrink]) {
                Message = "Please take your change";
                System.out.println(Message);
                cook();
            } else if (cash == prices[nDrink]) {
                Message = "Your drink will be ready in an instant";
                cook();
            }
        }
        return Message;
    }

    protected STATES cook() {
        if (state == STATES.CHECK) {
            state = STATES.COOK;
            printState();

            finish();
        }
        return state;
    }

    public String finish() {
        if (state == STATES.COOK) {
            Message = "Your drink is ready";
            System.out.println(Message);
            state = STATES.WAIT;
            printState();
        }
        return Message;
    }
}

    
    

