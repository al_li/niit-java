/*
Лабораторный практикум №5
Проект AutomataGUIDemo
Разработать версию автомата с использованием графического 
пользовательского интерфейса
 */

import javax.swing.JProgressBar;

public class demoAvtomat {

    public static void main(String[] args) {
        UserInterfAvtom interfUser = new UserInterfAvtom();
        interfUser.setLocationRelativeTo(null);
        interfUser.setTitle("Автомат напитков");

        JProgressBar pb = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
        interfUser.add(pb);
        interfUser.setVisible(true);
        avtomat automat = new avtomat();
        // automat.on();
        // automat.coin(35);
        // automat.choise(2);
        //automat.check();
        //automat.cook();
        //automat.finish();
    }
}
