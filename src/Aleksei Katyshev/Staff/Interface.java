
interface Project {

    double MAX_CON_MANAGER=5;//5%
    double MIN_CON_PROGRAMMER=3;
    double MAX_CON_PROGRAMMER=7;
    double MIN_CON_TESTER=2;
    double MAX_CON_TESTER=5;
    double calcPaymentForContribution(double buget,double summ);
}

interface Heading {
    int LEADS_PM=18;//10 prog+5 test+1 TL+2 manager
    int LEADS_TL=10;//10 prog
    int LEADS_SM=2;
    double PAY_FOR_ONE_PM=5000;//плата за руководство project manager
    double PAY_FOR_ONE_TL=3000;//плата за руководство team leader
    double PAY_FOR_ONE_SM=30000;//senior
    void calcPaymentForHeading();
}

interface WorkTime {
    int WORK_TIME_TESTERS=160;//на проэкт тестерам выделено 160ч(каждому)
    int WORK_TIME_PROGRAMMERS=160;// на проэкт программистам выделено 160ч(каждому)
    int WORK_TIME_CLEANER=80;// 80ч в мес(каждому)
    int WORK_TIME_DRIVER=80;// 80ч в мес(каждому)
    double PAYMENT_FOR_HOUR_TESTERS=70;
    double PAYMENT_FOR_HOUR_PROGRAMMERS=120;
    double PAAYMENT_FOR_HOUR_CLEANER=100;
    double PAAYMENT_FOR_HOUR_DRIVER=125;
    void calcPaymentForWorkTime();
}

