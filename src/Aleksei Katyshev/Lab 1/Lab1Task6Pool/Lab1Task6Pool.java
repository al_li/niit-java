import java.math.BigDecimal;

public class Lab1Task6Pool{
	public static void main(String[] args){
	double poolRadius=3;
	double ferencePool;
	Circles myCircle=new Circles();
	myCircle.setRadius(poolRadius);
	myCircle.printIt();
	ferencePool=myCircle.getFerence();
	myCircle.setFerence(0);
	myCircle.setArea(0);
	myCircle.setRadius(poolRadius+1);
	myCircle.printIt();
	
	System.out.printf("Fence price = %.2f rub\n",(myCircle.getFerence()*2000));
	System.out.printf("Walkway price = %.2f rub\n",((myCircle.getArea()-ferencePool)*1000));
	}
}

class Circles{
	private double radius=0;	//= ference/2*Pi = корень из area/Pi
	private double ference=0; //окружность =2*Pi*radius
	private double area=0;	//площадь =Pi*(radius*radius)
	
	public void setRadius(double myRad){
		radius=myRad;
	}
	public void setFerence(double myFer){
		ference=myFer;
	}
	public void setArea(double myArea){
		area=myArea;
	}
	
	public double getRadius(){
		return radius;
	}
	public double getFerence(){
		return ference;
	}
	public double getArea(){
		return area;
	}
	
	private boolean checkComponents(){
		if (radius==0){
			if (ference!=0)
				radius=ference/(2*3.14);
			else if (area!=0)
				radius=Math.sqrt(area/3.14); 
			else {
				System.out.println("Error 1: no settings!");
				return(false);
			}
		}
		if (ference==0)
			ference=2*3.14*radius;
		if(area==0)
			area=3.14*Math.pow(radius,2);
		return true;
	}
	
	public void printIt(){
		if(checkComponents()){
		
			BigDecimal radiusBD=new BigDecimal(radius);
			System.out.println("Radius= "+radiusBD.setScale(2,BigDecimal.ROUND_HALF_UP));
			BigDecimal ferenceBD=new BigDecimal(ference);
			System.out.println("Ference= "+ferenceBD.setScale(2,BigDecimal.ROUND_HALF_UP));
			BigDecimal areaBD=new BigDecimal(area);
			System.out.println("Area= "+areaBD.setScale(2,BigDecimal.ROUND_HALF_UP));

		}
		else
			System.out.println("Check Components = false");
	}
	

}


