package MyNet;
import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;
public class Server {

    public static void main(String[] args) throws IOException {
        System.out.println("Старт сервера");
        BufferedReader in = null;//поток для чтения данных
        PrintWriter    out= null;//поток для отправки данных

        ServerSocket server = null;//серверный сокет(порт)
        Socket       client = null;//сокет для клиента

        /** создаем серверный сокет*/
        try {
            server = new ServerSocket(1234);
        } catch (IOException e) {
            System.out.println("Ошибка связывания с портом 1234");
            System.exit(-1);
        }
        /**создаём клиент сокет*/
        try {
            System.out.print("Ждем соединения");
            client= server.accept();
            System.out.println("Клиент подключился");
        } catch (IOException e) {
            System.out.println("Не могу установить соединение");
            System.exit(-1);
        }
        /**создаём потоки для связи с клиентом  */
        in  = new BufferedReader(
                new InputStreamReader(client.getInputStream()));
        out = new PrintWriter(client.getOutputStream(),true);
        String input;
        /**цикл ожидания сообщений*/
        System.out.println("Ожидаем сообщений");
        while ((input = in.readLine()) != null) {
            Date date=new Date();
            SimpleDateFormat dateToday=new SimpleDateFormat("E dd.MM.yyyy G");
            SimpleDateFormat timeNow=new SimpleDateFormat("HH:mm:ss");
            System.out.println(input);
            if (input.equalsIgnoreCase("exit"))
                break;
            if (input.equalsIgnoreCase("dateS"))
                out.println(dateToday.format(date));
            if (input.equalsIgnoreCase("timeS"))
                out.println(timeNow.format(date));
        }
        /**закрываем соединения*/
        out.close();
        in.close();
        client.close();
        server.close();
    }
}
