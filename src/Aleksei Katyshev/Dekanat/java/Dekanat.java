import java.lang.String;
import Classes.*;
import java.util.List;

public class Dekanat {

    public static void main(String [] args){
        List<Student> students;
        List<Group> groups;

        Classes.Dekanat dekan=new Classes.Dekanat();
        students=dekan.getStudents();
        groups=dekan.getGroups();
        dekan.putMarks();
        dekan.getStatistics();
        students= dekan.transferStudent(students.get(23),"ЭКБ-30");
        groups=dekan.getGroups();
        dekan.checkAcademicPerformance();
        for(Group group:groups) {
            dekan.election(group);
        }
        dekan.writeFile();
    }


    }

