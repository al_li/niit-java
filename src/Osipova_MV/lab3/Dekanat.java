package Dekanat;

/*
Примерный перечень полей:

Students - массив студентов
Groups - массив групп
Обеспечить класс следующими методами:

создание студентов на основе данных из файла
создание групп на основе данных из файла
добавление случайных оценок студентам
накопление статистики по успеваемости студентов и групп
перевод студентов из группы в группу
отчисление студентов за неуспеваемость
сохранение обновленных данных в файлах
инициация выборов старост в группах
вывод данных на консоль
*/

import java.io.*;

public class Dekanat {
    private int maxStudentsNumber = 50;
    private int maxGroupsNumber = 5;
    private Student[] Students = new Student[maxStudentsNumber];
    int studentsCount = 0;
    private Group[] Groups = new Group[maxGroupsNumber];
    int groupsCount = 0;

    //Загрузка студентов из файла
    public void loadStudents (String file){
        String fio;
        try {
            File fileStudent = new File(file);
            FileReader studentReader = new FileReader(fileStudent);
            BufferedReader bStudentReader = new BufferedReader(studentReader);
            while ((fio = bStudentReader.readLine()) != null){
                Students[studentsCount] = new Student(fio,studentsCount+1);
                studentsCount++;
            }
            studentReader.close();
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("Error: file " + file + " not found.");
        }
        catch (IOException ioe) {
            System.out.println("Error: can't read file.");
        }
    }

    //Загрузка групп из файла
    public void loadGroups (String file) {
        String title;
        try {
            File fileGroup = new File(file);
            FileReader groupReader = new FileReader(fileGroup);
            BufferedReader bGroupReader = new BufferedReader(groupReader);
            while((title = bGroupReader.readLine()) != null) {
                Groups[groupsCount] = new Group(title);
                groupsCount++;
            }
            groupReader.close();
        }
        catch (FileNotFoundException fnfe) {
            System.out.println("Error: file " + file + " not found.");
        }
        catch (IOException ioe) {
            System.out.println("Error: can't read file");
        }
    }

    //Отчислить студента
    public void deleteStudent(Student student){
        for (int i = 0; i<groupsCount; i++) {

        }
    }

    //Раскидать студентов по группам в случайном порядке
    public void addStudentsToRandomGroups() {
        int rnd = 0;
        for(int i = 0;i<studentsCount;i++){
            rnd = (int)(Math.random()* groupsCount);
            Groups[rnd].addStudent(Students[i]);
        }
    }

    //заполняем дневники студентов случайными оценками
    public void addRandomMarks() {
        int marksNumber = 5;
        for(int i = 0; i<studentsCount; i++){
            for(int j = 0;j < marksNumber;j++){
                int random = 2 + (int) (Math.random() * 5);
                Students[i].addMark(random);
            }
        }
    }

    //Перевод студента из группы в группу
    public void changeGroup(int studentId,String newGroup) {
        for (int i = 0; i<studentsCount; i++){
            Student student = Students[i];
            if (student.getId() == studentId){
                //Ищем группу по названию
                for(int j=0;j<groupsCount;j++){
                    if (Groups[j].getTitle().equals(newGroup)){
                        //Удаляем из старой
                        student.getGroup().delStudent(student);
                        //Добавляем в новую
                        Groups[j].addStudent(student);
                    }
                }
            }
        }
    }

    //Отчисление студентов за неуспеваемость
    public void deleteStupidStudents(){
        for(int i=0; i<studentsCount; i++){
            if(Students[i].getAverageMark()<2.5){
                Students[i].getGroup().delStudent(Students[i]);
            }
        }
    }

    //Средняя оценка по группам
    public double getAverageGroupsMark(){
        double sum = 0;
        for(int i =0; i<groupsCount; i++){
            sum = sum + Groups[i].getAverageMark();
        }
        return sum/groupsCount;
    }

    //Вывод статистики по успеваемости студентов и групп
    public void printStatistic(){
        System.out.println("Statistics:");
        System.out.println("Item:\tAverage mark");
        for(int i =0; i<studentsCount;i++){
            System.out.println("Student:" + Students[i].getFio() + "\t" + Students[i].getAverageMark());
        }
        for(int i =0; i<groupsCount; i++){
            System.out.println("Group:" + Groups[i].getTitle() + "\t" + Groups[i].getAverageMark());
        }
        System.out.println("Dekanat:\t" + getAverageGroupsMark());
        System.out.println();
    }

    //Выбираем старосту в нужной группе
    public void setRandomHeads(){
        for (int i = 0; i<groupsCount;i++){
            Groups[i].setRandomHead();
        }
    }

    //Поиск по имени
    public Student findStudentByName(String name){
        for(int i=0;i<studentsCount;i++){
            if(name.equals(Students[i].getFio())) {
                return Students[i];
            }
        }
        return null;
    }

    //Поиск по ID
    public Student findStudentByID(int id){
        for(int i=0;i<studentsCount;i++){
            if(id == Students[i].getId()) {
                return Students[i];
            }
        }
        return null;
    }

    //вывод данных на консоль(имя студента - группа)
    public void print() {
        for(int i =0; i<groupsCount;i++){
            Groups[i].print();
            System.out.println();
        }
        System.out.println("Students without groups:");
        System.out.println("ID:\tName:");
        for(int i = 0; i<studentsCount; i++){
            if(Students[i].getGroup() == null){
                Students[i].print();
            }
        }
        System.out.println();
    }

    //запись результата в файл
    void writeResult(String file){
        try {
            Student temp;
            FileWriter fw = new FileWriter(file);
            fw.write("Name:\tGroup\tAverageMark\n");
            for (int i=0; i<studentsCount; i++){
                fw.write(Students[i].getFio());
                fw.write("\t");
                fw.write(Students[i].getGroup().getTitle());
                fw.write("\t");
                fw.write(Double.toString(Students[i].getAverageMark()));
                fw.write("\n");
                i++;
            }
            fw.close();
        }
        catch (IOException ioe){
            System.out.println("Error,can't write in file");
        }
    }

}
