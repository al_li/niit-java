package Dekanat;

/*
Примерный перечень полей:

ID - идентификационный номер
Fio - фамилия и инициалы
Group - ссылка на группу (объект Group)
Marks - массив оценок
Num - количество оценок
Обеспечить класс следующими методами:

создание студента с указанием ИД и ФИО
зачисление в группу
добавление оценки
вычисление средней оценки
*/

public class Student {
    private int marksNumber = 10;
    private int num = 0;
    private String Fio;
    private int id;
    private int[] marks = new int[marksNumber];
    private Group studentGroup;


    //Конструктор класса
    Student(String Fio,int id){
        this.Fio = Fio;
        this.id = id;
    }
    //Добавление оценки
    public void addMark(int mark){
        marks[num] = mark;
        num++;
    }
    //Зачисление в группу
    public void setGroup(Group group){
        studentGroup = group;
    }
    //Средняя оценка
    public double getAverageMark(){
        double sum = 0;
        for(int i=0;i<num;i++)
            sum = sum + marks[i];
        return sum / num;
    }
    public Group getGroup(){
        return studentGroup;
    }
    public String getFio(){
        return Fio;
    }
    public int getId(){
        return id;
    }
    public void print(){
        System.out.println(id + "\t" + Fio);
    }
}