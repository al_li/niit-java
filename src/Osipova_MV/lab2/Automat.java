import java.util.Arrays;

public class Automat {
    int cash = 0;
    MenuItem[] menu = new MenuItem[] {
            new MenuItem("Tea", Drinks.TEA, 20),
            new MenuItem("Espresso", Drinks.ESPRESSO, 25),
            new MenuItem("Latte", Drinks.LATTE, 25),
            new MenuItem("Chocolate", Drinks.CHOCOLATE, 35)
    };
    MenuItem userChoise;
    enum States {OFF, WAIT, ACCEPT, CHEK, COOK};
    States state;

    //включение автомата
    public void on() {
        state = States.WAIT;
        printMenu();
        System.out.println("Автомат включен");
    }

    //выключение автомата
    public void off() {
        state = States.OFF;
        System.out.println("Автомат выключен");
    }

    //занесение денег на счет пользователем
    public void coin(int coin) {
        if (state == States.WAIT|| state == States.ACCEPT) {
        state = States.ACCEPT;
        cash = +coin;
        }
    }

    // отображение меню с напитками и ценами для пользователя
    public void printMenu() {
        System.out.println("Menu:");
        for(int i=0;i<menu.length;i++) {
            System.out.println(menu[i].ItemName);
        }
        System.out.println();
    }

    // отображение текущего состояния для пользователя
    public States printState() {
        return state;
    }

    // выбор напитка пользователем
    public boolean choice(Drinks drink) {
        for(int i=0;i<menu.length;i++) {
            if(menu[i].Drink == drink){
                userChoise = menu[i];
            }
        }
        return check();
    }

    // проверка наличия необходимой суммы;
    public boolean check() {
        boolean retVal = false;
        if (cash < userChoise.Price) {
            cancel();
        } else {
            state = States.COOK;
            cook();
            retVal = true;
        }

        return retVal;
    }

    //отмена сеанса обслуживания пользователем
    public void cancel() {
        state = States.WAIT;
        System.out.println("Сеанс отменен пользователем");
    }

    //имитация процесса приготовления напитка
    public void cook() {
        System.out.printf("Готовим %s\n", userChoise.ItemName);
        finish();
    }


    //завершение обслуживания пользователя.
    public void finish() {
        state = States.WAIT;
        System.out.printf("Возьмите Ваш %s\n", userChoise.ItemName);
    }

    // Возврат денег
    public int returnMoney() {
        int delivery = 0;
        if (state != States.COOK) {
            delivery = cash;
            cash = 0;
            state = States.WAIT;
        }
        return delivery;
    }
}
