package Staff;

abstract class Personal extends Employee implements WorkTime
{
    Personal(int id, String name, int workTime, int payment){
        super(id, name, workTime, payment);
    }

    public int calcWorkTimePayment(){
        return workTime * payment;
    }

    @Override
    public int calcPayment(){
        return calcWorkTimePayment();
    }
}